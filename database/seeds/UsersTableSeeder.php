<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'  => 'admin',
            'email' => 'admin' . '@admin.com',
            'password'  => bcrypt('admin123'),
            'jabatan' => 'Admin Gudang',
            'no_telp' => '085712345678',
        ]);
    }
}
