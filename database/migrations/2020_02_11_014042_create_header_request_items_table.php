<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeaderRequestItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_permintaan_barang', function (Blueprint $table) {
            Schema::dropIfExists('header_permintaan_barang');
            $table->string('nomor_surat')->primary();
            $table->date('tgl_permintaan');
            $table->text('keterangan');
            $table->bigInteger('kantor_id')->unsigned();
            $table->string('file_bukti')->nullable();
            $table->string('status');;
            $table->integer('vallidasi')->default(0);
            $table->bigInteger('created_by')->unsigned();
            $table->integer('updated_by')->nullable();
            $table->foreign('kantor_id')->references('id')->on('kantor')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_permintaan_barang');
    }
}
