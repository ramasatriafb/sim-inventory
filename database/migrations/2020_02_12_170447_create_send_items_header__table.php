<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSendItemsHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_pengiriman_barang', function (Blueprint $table) {
            Schema::dropIfExists('header_pengiriman_barang');
            $table->string('nomor_surat')->primary();
            $table->date('tgl_surat');
            $table->bigInteger('kantor_id')->unsigned();
            $table->text('keterangan');
            $table->string('status')->default('Dalam Proses');
            $table->bigInteger('penerima')->unsigned();
            $table->bigInteger('pengirim')->unsigned();
            $table->string('file_bukti')->nullable();
            $table->string('permintaan_id')->default(NULL);
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->nullable()->unsigned();;
            $table->integer('deleted_by')->nullable();
            $table->foreign('kantor_id')->references('id')->on('kantor')->onDelete('restrict');
            $table->foreign('penerima')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('pengirim')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('permintaan_id')->references('nomor_surat')->on('header_permintaan_barang')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('restrict');
            $table->timestamps();
        });
    }

/**creatt
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_pengiriman_barang');
    }
}
