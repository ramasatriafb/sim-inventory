<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecapRenstByUserViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW rekappeminjamanuserView AS
                       SELECT users.id, users.name, users.jabatan,users.no_telp, users.photo,COUNT(peminjaman_barang.kode_barang) as jumlah from users 
                        left outer join (
                            SELECT peminjam.id, peminjam.name as peminjam,barang.kode_barang
                            from header_peminjaman_barang 
                            join detail_peminjaman_barang on header_peminjaman_barang.nomor_surat = detail_peminjaman_barang.nomor_surat 
                            join barang on detail_peminjaman_barang.kode_barang = barang.kode_barang 
                            join users as peminjam on header_peminjaman_barang.peminjam = peminjam.id
                            join proyek on header_peminjaman_barang.proyek_id = proyek.id
                             where barang.dipinjam = 1
                        ) as peminjaman_barang on peminjaman_barang.id = users.id
                        group by users.name, users.id,  users.jabatan ,users.no_telp, users.photo");
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW rekappeminjamanuserView");
    }
}
