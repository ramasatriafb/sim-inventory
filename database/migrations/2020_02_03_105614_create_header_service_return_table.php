<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeaderServiceReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_barang_service_kembali', function (Blueprint $table) {
            Schema::dropIfExists('header_barang_service_kembali');
            $table->bigIncrements('id');
            $table->date('tgl_pengembalian');
            $table->text('keterangan')->nullable();
            $table->bigInteger('penanggung_jawab')->unsigned();
            $table->bigInteger('pengembali_barang')->unsigned();
            $table->bigInteger('created_by')->unsigned();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->foreign('penanggung_jawab')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('pengembali_barang')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_barang_service_kembali');
    }
}
