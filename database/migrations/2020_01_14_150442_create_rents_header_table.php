<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentsHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('header_peminjaman_barang', function (Blueprint $table) {
            Schema::dropIfExists('header_peminjaman_barang');
            $table->string('nomor_surat')->primary();
            $table->date('tgl_surat');
            $table->bigInteger('proyek_id')->unsigned();
            $table->text('keterangan');
            $table->bigInteger('penanggung_jawab')->unsigned();
            $table->bigInteger('peminjam')->unsigned();
            $table->integer('aktif')->nullable();
            $table->string('file_bukti')->nullable();
            $table->bigInteger('created_by')->unsigned();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->foreign('proyek_id')->references('id')->on('proyek')->onDelete('restrict');
            $table->foreign('penanggung_jawab')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('peminjam')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_peminjaman_barang');
    }
}
