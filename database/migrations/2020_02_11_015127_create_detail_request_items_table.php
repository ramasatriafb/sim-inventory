<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailRequestItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_permintaan_barang', function (Blueprint $table) {
            Schema::dropIfExists('detail_permintaan_barang');
            $table->bigIncrements('detail_id')->unsigned();
            $table->string('nomor_surat');
            $table->string('kode_barang');
            $table->bigInteger('created_by')->unsigned();
            $table->integer('updated_by')->nullable();
            $table->foreign('nomor_surat')->references('nomor_surat')->on('header_permintaan_barang')->onDelete('restrict');
            $table->foreign('kode_barang')->references('kode_barang')->on('barang')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_permintaan_barang');
    }
}
