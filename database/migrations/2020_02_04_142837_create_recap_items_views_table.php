<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecapItemsViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW rekapbarangView AS
                       SELECT DISTINCT kategori_inventaris.kategori as kategori, jenis_barang.jenis as jenis , barang.kode_barang , barang.nama_barang, barang.merk,
                        barang.status, barang.dipinjam as ketersediaan, barang.tgl_pembelian,
                        (CASE 
                            WHEN barang.dipinjam = 1 then peminjaman.peminjam
                        ELSE '-'
                        END ) as peminjam,
                        (CASE 
                            WHEN barang.dipinjam = 1 then peminjaman.penanggung_jawab
                        ELSE '-'
                        END ) as penanggung_jawab

                
                        FROM barang
                        left outer join
                        (SELECT detail_peminjaman_barang.kode_barang as kd, peminjam.name as peminjam, penanggungjawab.name as penanggung_jawab 
                        FROM header_peminjaman_barang 
                        join detail_peminjaman_barang on header_peminjaman_barang.nomor_surat = detail_peminjaman_barang.nomor_surat 
                        join barang on detail_peminjaman_barang.kode_barang = barang.kode_barang 
                        join users as peminjam on header_peminjaman_barang.peminjam = peminjam.id 
                        join users AS penanggungjawab on header_peminjaman_barang.penanggung_jawab = penanggungjawab.id
                        join proyek on header_peminjaman_barang.proyek_id = proyek.id
                        ) as peminjaman on barang.kode_barang = peminjaman.kd

                        join jenis_barang on barang.jenisbarang_id = jenis_barang.id
                        join kategori_inventaris on barang.kategori_id = kategori_inventaris.id

                        GROUP by kategori_inventaris.kategori,jenis_barang.jenis , barang.kode_barang , barang.nama_barang, barang.merk,barang.status, barang.dipinjam, barang.tgl_pembelian, peminjaman.peminjam, peminjaman.penanggung_jawab

                        ORDER by kategori_inventaris.kategori,jenis_barang.jenis, barang.tgl_pembelian desc");
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW rekapbarangView");
    }
}
