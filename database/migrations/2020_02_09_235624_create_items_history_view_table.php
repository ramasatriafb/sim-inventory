<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsHistoryViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW rekapriwayatBarang AS
        SELECT riwayat_barang.kode_barang, riwayat_barang.riwayat,info_user.name as info_user, riwayat_barang.tanggal FROM riwayat_barang 
        JOIN users as info_user on riwayat_barang.user_id = info_user.id
        ORDER BY riwayat_barang.tanggal DESC");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW rekapriwayatBarang");
    }
}
