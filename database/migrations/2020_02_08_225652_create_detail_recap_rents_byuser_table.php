<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailRecapRentsByuserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW detailrekappeminjamanuserView AS
        SELECT peminjam.id, peminjam.name as peminjam,barangView.kode_barang, barangView.nama_barang, barangView.merk, barangView.kategori, barangView.jenis, barangView.kelengkapan,barangView.foto_barang
                            from header_peminjaman_barang 
                            join detail_peminjaman_barang on header_peminjaman_barang.nomor_surat = detail_peminjaman_barang.nomor_surat 
                            join barangView on detail_peminjaman_barang.kode_barang = barangView.kode_barang 
                            join users as peminjam on header_peminjaman_barang.peminjam = peminjam.id
                            join proyek on header_peminjaman_barang.proyek_id = proyek.id
                            where barangView.dipinjam = 1 ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW detailrekappeminjamanuserView");
    }
}
