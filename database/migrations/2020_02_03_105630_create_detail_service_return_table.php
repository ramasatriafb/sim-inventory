<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailServiceReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_barang_service_kembali', function (Blueprint $table) {
            Schema::dropIfExists('detail_barang_service_kembali');
            $table->bigIncrements('detail_id');
            $table->bigInteger('barang_service_id')->unsigned();;
            $table->string('kode_barang');
            $table->string('status');
            $table->string('file_bukti')->nullable();
            $table->bigInteger('created_by')->unsigned();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->foreign('barang_service_id')->references('id')->on('header_barang_service_kembali')->onDelete('restrict');
            $table->foreign('kode_barang')->references('kode_barang')->on('barang')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_barang_service_kembali');
    }
}
