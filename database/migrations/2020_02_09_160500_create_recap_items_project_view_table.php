<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecapItemsProjectViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW rekappeminjamanProyekView AS
        SELECT proyek.id, proyek.nama_proyek, proyek.lokasi , proyek.penanggung_jawab, COUNT(peminjaman_barang.kode_barang) as jumlah
         FROM proyek 
            LEFT OUTER join (
	            SELECT proyek.id, proyek.nama_proyek,barang.kode_barang
                            from header_peminjaman_barang 
                            join detail_peminjaman_barang on header_peminjaman_barang.nomor_surat = detail_peminjaman_barang.nomor_surat 
                            join barang on detail_peminjaman_barang.kode_barang = barang.kode_barang 
                            join users as peminjam on header_peminjaman_barang.peminjam = peminjam.id
                            join proyek on header_peminjaman_barang.proyek_id = proyek.id
                             where barang.dipinjam = 1
                ) as peminjaman_barang on peminjaman_barang.id = proyek.id
                group by proyek.id, proyek.nama_proyek, proyek.lokasi , proyek.penanggung_jawab");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW rekappeminjamanProyekView");
    }
}
