<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllRecapRentsByUserViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW semuarekappeminjamanuserView AS
        SELECT distinct peminjam.id, peminjam.name as peminjam,barang.kode_barang, barang.nama_barang, barang.merk, barang.kelengkapan,barang.foto_barang, header_peminjaman_barang.tgl_surat, barang.dipinjam
        from header_peminjaman_barang 
        join detail_peminjaman_barang on header_peminjaman_barang.nomor_surat = detail_peminjaman_barang.nomor_surat 
        join barang on detail_peminjaman_barang.kode_barang = barang.kode_barang 
        join users as peminjam on header_peminjaman_barang.peminjam = peminjam.id
        join proyek on header_peminjaman_barang.proyek_id = proyek.id  
        ORDER BY header_peminjaman_barang.tgl_surat DESC ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW semuarekappeminjamanuserView");
    }
}
