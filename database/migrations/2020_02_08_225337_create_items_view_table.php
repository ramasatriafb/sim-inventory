<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW barangView AS
        SELECT barang.kode_barang, kategori_inventaris.kategori,jenis_barang.jenis,barang.nama_barang, barang.merk, barang.kelengkapan, barang.foto_barang, barang.dipinjam from barang 
              join kategori_inventaris on barang.kategori_id = kategori_inventaris.id
              join jenis_barang on barang.jenisbarang_id = jenis_barang.id ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW barangView");
    }
}
