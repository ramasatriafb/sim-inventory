<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceItemsHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_perbaikan_barang', function (Blueprint $table) {
            Schema::dropIfExists('header_perbaikan_barang');
            $table->string('nomor_surat')->primary();
            $table->date('tgl_surat');
            $table->text('keterangan');
            $table->string('nama_tempat_perbaikan')->nullable();;
            $table->text('alamat_perbaikan')->nullable();;
            $table->bigInteger('penanggung_jawab')->unsigned();
            $table->bigInteger('pihak_pembawa')->unsigned();
            $table->integer('aktif')->nullable();
            $table->string('file_bukti')->nullable();
            $table->bigInteger('created_by')->unsigned();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->foreign('penanggung_jawab')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('pihak_pembawa')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_perbaikan_barang');
    }
}
