<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function(Blueprint $table){
            Schema::dropIfExists('barang');
            $table->string('kode_barang')->unique()->primary();
            $table->bigInteger('kategori_id')->unsigned();
            $table->bigInteger('jenisbarang_id')->unsigned();
            $table->string('nama_barang');
            $table->string('merk')->nullable();;
            $table->mediumText('kelengkapan')->nullable();
            $table->string('status');
            $table->string('foto_barang')->default('items.png');
            $table->date('tgl_pembelian')->nullable();
            $table->string('file_invoice');
            $table->bigInteger('kantor_id')->unsigned();
            $table->mediumText('keterangan')->nullable();
            $table->integer('dipinjam')->default(0);
            $table->bigInteger('created_by')->unsigned();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('kategori_id')->references('id')->on('kategori_inventaris')->onDelete('restrict');
            $table->foreign('jenisbarang_id')->references('id')->on('jenis_barang')->onDelete('restrict');
            $table->foreign('kantor_id')->references('id')->on('kantor')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
