<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_barang', function (Blueprint $table) {
            Schema::dropIfExists('riwayat_barang');
            $table->bigIncrements('id');
            $table->string('kode_barang');
            $table->text('riwayat');
            $table->bigInteger('user_id')->unsigned();
            $table->date('tanggal');;
            $table->foreign('kode_barang')->references('kode_barang')->on('barang')->onDelete('restrict');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_barang');
    }
}
