<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Office

Route::apiResources(['kantor' => 'API\OfficeController']);
Route::get('getDataKantor', 'API\OfficeController@getDataKantor');
Route::get('getKantor', 'API\OfficeController@getKantor');

// Log Activity

Route::get('logActivity', 'API\LogActivityController@logActivity');
Route::get('findLogs', 'API\LogActivityController@search');
Route::get('filterLog/{tglawal}/{tglakhir}', 'API\LogActivityController@filterLog');

//User

Route::apiResources(['user' => 'API\UserController']);
Route::get('findUser', 'API\UserController@search');
Route::get('profile', 'API\UserController@profile');
Route::put('profile', 'API\UserController@updateProfile');
Route::get('getUser', 'API\UserController@getUser');
Route::get('getUserAdmin', 'API\UserController@getUserAdmin');
Route::get('adminlocation', 'API\UserController@getaAdminLocation');
Route::get('adminoffice', 'API\UserController@getaAdminLocationOffice');
Route::get('userid', 'API\UserController@getaUserID');

// Reference Number

Route::apiResources(['nomorsurat' => 'API\ReferenceNumberController']);
Route::get('getnomorsurat/{kategori}','API\ReferenceNumberController@getReferenceNumber');

//Category

Route::apiResources(['kategori' => 'API\CategoryController']);
Route::get('findKategori', 'API\CategoryController@search');
Route::get('getDataKategoriInventaris', 'API\CategoryController@getDataKategoriInventaris');

// Type

Route::apiResources(['jenis' => 'API\TypeController']);
Route::get('findJenis', 'API\TypeController@search');
Route::get('getDataJenisBarang', 'API\TypeController@getDataJenisBarang');


// Project

Route::apiResources(['proyek' => 'API\ProjectController']);
Route::get('findProyek', 'API\ProjectController@search');
Route::get('getProyek', 'API\ProjectController@getProyek');

// Items


Route::apiResources(['barang' => 'API\ItemsController']);
Route::get('findBarang', 'API\ItemsController@search');
Route::get('filterBarang/{kategori}/{jenis}/{status}', 'API\ItemsController@filter');
Route::get('findBarangHapus', 'API\ItemsController@searchDeleted');
Route::get('filterBarangHapus/{kategori}/{jenis}/{status}', 'API\ItemsController@filterDeleted');
Route::get('barang-dihapus', 'API\ItemsController@trash');
Route::delete('hapusPermanent/{id}','API\ItemsController@deletePermanent');
Route::get('/barangdirestore/{id}', 'API\ItemsController@restore');
Route::get('/getdataExcel', 'API\ItemsController@getDataExcel');
Route::get('/getDataBarangAvailable', 'API\ItemsController@getDataBarangAvailable');
Route::get('/getDataBarangAvailable/{id}', 'API\ItemsController@getDataBarangAvailable');
Route::get('/getItemsBorrowed', 'API\ItemsController@getDataBarangBorrowed');
Route::get('findBarangAvailable', 'API\ItemsController@searchAvailable');
Route::get('findItemsBorrowed', 'API\ItemsController@searchBorrowed');
Route::get('/getDataBarangService', 'API\ItemsController@getDataBarangService');
Route::get('findBarangService', 'API\ItemsController@searchService');

Route::get('/getDataBarangRusak', 'API\ItemsController@getDataBarangRusak');
Route::get('findBarangBroke', 'API\ItemsController@searchBroke');

// Export Excel Items
Route::get('/items/export_excel', 'API\ItemsController@export_excel');

// Dashboard
Route::get('dashboardBarang', 'API\ItemsController@getDataDashboard');
Route::get('dashboardBarangByOffice', 'API\ItemsController@getDataDashboardByOffice');
Route::get('statisticItems', 'API\ItemsController@statisticItems');
Route::get('statisticItemsOffice', 'API\ItemsController@statisticItemsOffice');
Route::get('rentsItemsProject', 'API\RentsItemsController@getRentsItemsByProject');

// Rents Items

Route::apiResources(['peminjamanbarang' => 'API\RentsItemsController']);
Route::get('getDataPeminjaman/{id}', 'API\RentsItemsController@getDataPeminjaman');
Route::get('findPeminjaman', 'API\RentsItemsController@search');
Route::get('getDetailPeminjaman/{id}', 'API\RentsItemsController@getDetailPeminjaman');
Route::get('filterPeminjaman/{proyek}/{tglawal}/{tglakhir}', 'API\RentsItemsController@filter');
Route::put('uploadBuktiPinjam/{id}','API\RentsItemsController@uploadBukti');
Route::get('getHeaderPeminjaman/{id}', 'API\RentsItemsController@getHeaderPeminjaman');


Route::delete('barangPinjam/{id}', 'API\RentsItemsController@deleteItemsRents');

// Return Items

Route::apiResources(['pengembalianbarang' => 'API\ReturnItemsController']);
Route::get('getDataPengembalian/{id}', 'API\ReturnItemsController@getDataPengembalian');
Route::get('findPengembalian', 'API\ReturnItemsController@search');
Route::get('getDetailPengembalian/{id}', 'API\ReturnItemsController@getDetailPengembalian');
Route::get('filterPengembalian/{proyek}/{tglawal}/{tglakhir}', 'API\ReturnItemsController@filter');
Route::put('uploadBuktiPengembalian/{id}','API\ReturnItemsController@uploadBukti');
Route::delete('barangPengembalian/{id}', 'API\ReturnItemsController@deleteItemsReturns');
Route::get('getHeaderPengembalian/{id}', 'API\ReturnItemsController@getHeaderPengembalian');

// Service Items

Route::apiResources(['perbaikanbarang' => 'API\ServiceItemsController']);
Route::get('getDataPerbaikan/{id}', 'API\ServiceItemsController@getDataPerbaikan');
Route::get('findPerbaikan', 'API\ServiceItemsController@search');
Route::get('getDetailPerbaikan/{id}', 'API\ServiceItemsController@getDetailPerbaikan');
Route::get('filterPerbaikan/{tglawal}/{tglakhir}', 'API\ServiceItemsController@filter');
Route::put('uploadBuktiPerbaikan/{id}','API\ServiceItemsController@uploadBukti');
Route::delete('barangPerbaikan/{id}', 'API\ServiceItemsController@deleteItemsService');
Route::get('getHeaderPerbaikan/{id}', 'API\ServiceItemsController@getHeaderPerbaikan');

// Service Items Return


Route::apiResources(['perbaikanbarangkembali' => 'API\ServiceReturnController']);
Route::get('getDetailPerbaikanSelesai/{id}', 'API\ServiceReturnController@getDetailPerbaikanSelesai');
Route::delete('barangSelesaiPerbaikan/{id}', 'API\ServiceReturnController@deleteItemsSelesaiPerbaikan');
Route::get('getHeaderSelesaiPerbaikan/{id}', 'API\ServiceReturnController@getHeaderSelesaiPerbaikan');
Route::get('filterSelesaiPerbaikan/{tglawal}/{tglakhir}', 'API\ServiceReturnController@filter');
Route::get('findSelesaiPerbaikan', 'API\ServiceReturnController@search');

// Request Items

Route::apiResources(['permintaanbarang' => 'API\RequestItemsController']);
Route::get('getDataPermintaan/{id}', 'API\RequestItemsController@getDataPermintaan');
Route::get('filterPermintaan/{tglawal}/{tglakhir}', 'API\RequestItemsController@filter');
Route::get('getHeaderPermintaan/{id}', 'API\RequestItemsController@getHeaderPermintaan');
Route::get('getDetailPermintaan/{id}', 'API\RequestItemsController@getDetailPermintaan');
Route::get('findPermintaan', 'API\RequestItemsController@search');

Route::delete('barangMinta/{id}', 'API\RequestItemsController@deleteItemsRequest');
Route::put('validasiPermintaan/{id}', 'API\RequestItemsController@validasiPermintaan');


Route::get('getPermintaan', 'API\RequestItemsController@getPermintaan');

// Send Items

Route::apiResources(['pengirimanbarang' => 'API\SendItemsController']);
Route::get('getDataPengiriman/{id}', 'API\SendItemsController@getDataPengiriman');
Route::get('getDetailPengiriman/{id}', 'API\SendItemsController@getDetailPengiriman');
Route::put('uploadBuktiKirim/{id}','API\SendItemsController@uploadBukti');
Route::get('findPengiriman', 'API\SendItemsController@search');
Route::get('filterPengiriman/{kantor}/{tglawal}/{tglakhir}', 'API\SendItemsController@filter');
Route::get('getHeaderPengiriman/{id}', 'API\SendItemsController@getHeaderPengiriman');
Route::delete('barangPengiriman/{id}', 'API\SendItemsController@deleteItemsSend');

Route::put('konfirmasiPenerimaan/{id}', 'API\SendItemsController@konfirmasiPenerimaan');

// Recap Items

Route::get('recapItems', 'API\ItemsController@recapItems');
Route::get('findRekapBarang', 'API\ItemsController@findRekapBarang');
Route::get('filterRekapBarang/{kategori}/{jenis}/{status}/{tglawal}/{tglakhir}', 'API\ItemsController@filterRekapBarang');
Route::get('/getdataExcelRecap', 'API\ItemsController@getdataExcelRecap');

// Recap Rents Item By User


Route::get('recapItemsUser', 'API\RentsItemsController@recapItemsUser');
Route::get('findrecapItemsUser', 'API\RentsItemsController@findrecapItemsUser');
Route::get('getDetailHistoryPeminjamanUser/{id}', 'API\RentsItemsController@getDetailHistoryPeminjamanUser');
Route::get('getHistoryPeminjamanUser/{id}', 'API\RentsItemsController@getHistoryPeminjamanUser');

// Recap Rents Item By Project


Route::get('recapItemsProject', 'API\RentsItemsController@recapItemsProject');
Route::get('findrecapItemsProject', 'API\RentsItemsController@findrecapItemsProject');
Route::get('getDetailHistoryPeminjamanProyek/{id}', 'API\RentsItemsController@getDetailHistoryPeminjamanProyek');
Route::get('getHistoryPeminjamanProyek/{id}', 'API\RentsItemsController@getHistoryPeminjamanProyek');

// Recap History Items

Route::get('recapHistoryItems/{id}', 'API\ItemsController@recapHistoryItems');