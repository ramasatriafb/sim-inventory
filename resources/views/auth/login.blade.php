@extends('layouts.app')

<style>
    .card{
        margin-top: 100px;
        height: 400px;
    }
    .titleApp{
        margin-top: 150px !important; 
        color: #FFF;
        text-align: left;
        font-size: 2.275rem
    }
    #background{
        background-color: #2a7ffff6;
        margin-top: -20px;
        margin-bottom: -20px;
        margin-left: -5px;
        height: 400px;
    }
    .nameApp{
        color: #FFF !important;
        font-weight: bold;
        text-align: left;
        margin-top: 5px;
        font-size: 2.275rem
    }

    /* Form */
    .logo{
        margin-left: 120px;
        margin-right: 50px;
        margin-top: 30px;
        margin-bottom: 30px;
        height: 75px;
        width: 75px;
    }
    .info{
        margin-top: -15px;
    }
</style>

@section('content')
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
               
                <div class="card-body">
                    <div class="row justify-content-center">

                        <div class="col-lg" id="background">
                                <h3 class="titleApp">Selamat datang di </h3>
                                <h3 class="nameApp">i - Vory
                        </div>
                        <div class="col-lg">
                        <img class="logo" src="{{asset('/img/asset/inventory.png')}}" alt="Inventory">
                        <h1><b>Login</b></h1>
                            <form method="POST" action="{{ route('login') }}">
                                    @csrf
            
                                    <div class="form-group row">
                                     
                                        <div class="col-md-12">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Alamat Email">
            
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
            
                                    <div class="form-group row">
                                     
                                        <div class="col-md-12">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
            
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
            
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Masuk') }}
                                            </button>
            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <p class="info">Jika Anda terkendala dengan login aplikasi,</p>
                                            <p class="info">harap hubungi admin</p>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
