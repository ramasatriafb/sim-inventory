<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>SIM Multi Inventory</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="/css/app.css">
  
  <link rel="shortcut icon" type="image/x-icon" href="./img/asset/inventory.png" />
  <style>
  .brand-link{
      text-align: center;
  }
  .brand-image{
      max-height: 60px;
      margin-left: 90px;
      margin-top: 5px;
  }
  .brand-text{
      font-weight: bold;
  }
  </style>
</head>
<body class="hold-transition sidebar-mini" >
<div class="wrapper" id="app">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
         
        </ul>
      </nav>
      <!-- /.navbar -->

  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="/img/asset/inventory.png" alt="Inventory Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
           <br><br><br>
      <span class="brand-text">i - Vory</span>
      <br><br>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
         <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img src="./img/profile/{{ Auth::user()->photo }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a href="#" class="d-block"><b>{{Auth::user()->name}}</b>
              <p>{{Auth::user()->name}}</p>
              </a>
            </div>
          </div>
    
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
            <router-link to="/dashboard" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                Dashboard
                </p>
            </router-link>
        </li>

            {{-- Managemen Sistem --}}
        @can('isAdmin')
        <li class="nav-item has-treeview">
            <a href="" class="nav-link">
                <i class="nav-icon fas fa-cogs"></i>
                <p>
                Manajemen Sistem
                <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <router-link to="/kantor" class="nav-link">
                        <i class="far fa-building nav-icon"></i>
                        <p>Kantor Cabang</p>
                    </router-link>
                    </li>
                <li class="nav-item">
                <router-link to="/penggunasistem" class="nav-link">
                    <i class="fas fa-user-cog nav-icon"></i>
                    <p>Pengguna Sistem</p>
                </router-link>
                </li>
                <li class="nav-item">
                <router-link to="/log" class="nav-link">
                    <i class="fas fa-book nav-icon"></i>
                    <p>Log Sistem</p>
                </router-link>
                </li>
             
                {{-- <li class="nav-item">
                  <router-link to="/developer" class="nav-link">
                      <i class="fas fa-cogs nav-icon"></i>
                      <p>Developer</p>
                  </router-link>
                </li> --}}
              
            </ul>
        </li>
        

          {{-- Master Data --}}
          <li class="nav-item has-treeview">
                <a href="" class="nav-link">
                    <i class="nav-icon fas fa-database"></i>
                    <p>
                    Master Data
                    <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <router-link to="/referensinomorsurat" class="nav-link">
                          <i class="right fas fa-angle-right"></i>
                          <p>Referensi Nomor Surat</p>
                        </router-link>
                      </li>
                    <li class="nav-item">
                      <router-link to="/kategori" class="nav-link">
                        <i class="right fas fa-angle-right"></i>
                        <p>Kategori Inventaris</p>
                      </router-link>
                    </li>
                    <li class="nav-item">
                      <router-link to="/jenisbarang" class="nav-link">
                        <i class="right fas fa-angle-right"></i>
                        <p>Jenis Barang</p>
                      </router-link>
                    </li>
                    <li class="nav-item">
                      <router-link to="/proyek" class="nav-link">
                        <i class="right fas fa-angle-right"></i>
                        <p>Proyek</p>
                      </router-link>
                    </li>
                    <li class="nav-item">
                      <router-link to="/barang" class="nav-link">
                        <i class="right fas fa-angle-right"></i>
                        <p>Barang</p>
                      </router-link>
                    </li>
                    </ul>
            </li>

          {{-- Form Inventaris --}}

        <li class="nav-item has-treeview ">
          <a href="" class="nav-link">
              <i class="nav-icon fab fa-wpforms"></i>
              <p>
              Form Inventaris
              <i class="right fas fa-angle-left"></i>
              </p>
          </a>
          <ul class="nav nav-treeview">
              <li class="nav-item">
              <router-link to="/formpeminjaman" class="nav-link">
                  <i class="fas fa-arrow-right nav-icon"></i>
                  <p>Peminjaman Barang</p>
              </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/formpengembalian" class="nav-link">
                  <i class="fas fa-arrow-left nav-icon"></i>
                  <p>Pengembalian Barang</p>
              </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/formperbaikan" class="nav-link">
                  <i class="fas fa-tools nav-icon"></i>
                  <p>Perbaikan Barang</p>
              </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/formselesaiperbaikan" class="nav-link">
                  <i class="far fa-thumbs-up nav-icon"></i>
                  <p>Selesai Perbaikan</p>
              </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/formpermintaanbarang" class="nav-link">
                  <i class="fas fa-exchange-alt nav-icon"></i>
                  <p>Permintaan Barang</p>
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/formpengirimanbarang" class="nav-link">
                  <i class="fas fa-paper-plane nav-icon"></i>
                  <p>Pengiriman Barang</p>
                </router-link>
              </li>
            </ul>
          </li>

          @endcan

          @can('isUser')
          {{-- Menu Master Barang --}}
        <li class="nav-item">
            <router-link to="/barang" class="nav-link">
                <i class="nav-icon fas fa-database"></i>
                <p>
                Daftar Barang
                </p>
            </router-link>
        </li>
        @endcan

          {{-- Riwayat Inventars --}}
        <li class="nav-item has-treeview">
            <a href="" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                Riwayat Inventaris
                <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <router-link to="/riwayatpeminjaman" class="nav-link">
                    <i class="fas fa-arrow-right nav-icon"></i>
                    <p>Peminjaman Barang</p>
                </router-link>
                </li>
                <li class="nav-item">
                <router-link to="/riwayatpengembalian" class="nav-link">
                    <i class="fas fa-arrow-left nav-icon"></i>
                    <p>Pengembalian Barang</p>
                </router-link>
                </li>
                <li class="nav-item">
                <router-link to="/riwayatperbaikan" class="nav-link">
                    <i class="fas fa-tools nav-icon"></i>
                    <p>Perbaikan Barang</p>
                </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/riwayatselesaiperbaikan" class="nav-link">
                    <i class="far fa-thumbs-up nav-icon"></i>
                    <p>Selesai Perbaikan</p>
                </router-link>
                </li>
                @can('isAdmin')
                <li class="nav-item">
                    <router-link to="/riwayatpermintaan" class="nav-link">
                      <i class="fas fa-exchange-alt nav-icon"></i>
                      <p>Permintaan Barang</p>
                    </router-link>
                </li>
                @endcan
                <li class="nav-item">
                  <router-link to="/riwayatpengiriman" class="nav-link">
                    <i class="fas fa-paper-plane nav-icon"></i>
                    <p>Pengiriman Barang</p>
                  </router-link>
                </li>
            </ul>
        </li>

        @can('isAdmin')
          {{-- Laporan Inventaris --}}

        <li class="nav-item has-treeview">
            <a href="" class="nav-link">
                <i class="nav-icon fas fa-book-open"></i>
                <p>
                Laporan Inventaris
                <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <router-link to="/rekapbarang" class="nav-link">
                  <i class="right fas fa-angle-right"></i>
                  <p>Rekap Barang</p>
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/riwayatpeminjamanuser" class="nav-link">
                  <i class="right fas fa-angle-right"></i>
                  <p>Riwayat Peminjaman User</p>
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/riwayatpeminjamanproyek" class="nav-link">
                  <i class="right fas fa-angle-right"></i>
                  <p>Riwayat Peminjaman Proyek</p>
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/riwayatbarang" class="nav-link">
                  <i class="right fas fa-angle-right"></i>
                  <p>Data Riwayat Barang</p>
                </router-link>
              </li>
            </ul>
        </li>
        @endcan
        {{-- Profil Pengguna --}}
        <li class="nav-item">
            <router-link to="/profile" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                Profil Pengguna
                </p>
            </router-link>
        </li>
          {{-- Keluar Aplikasi --}}
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                     
              <i class="nav-icon fas fa-sign-out-alt red"></i>  
              <p>     
              {{ __('Keluar Sistem') }}
              </p>
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="content-header">  
    <router-view></router-view>
    <vue-progress-bar></vue-progress-bar>
   </div>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
    Aplikasi Sim Inventory
    </div>
    <!-- Default to the left -->
    <strong>Powered By <a href="">Hips.Studio</a>  &copy; 2020 .</strong> Solusikan Dengan Digital
  </footer>
</div>
<!-- ./wrapper -->

@auth
<script>
  window.user = @json(auth()->user())
</script>
@endauth
<!-- REQUIRED SCRIPTS -->

<script src="/js/app.js"></script>
</body>
</html>
