export default class Gate {

    constructor(user){
        this.user = user;
    }

    isAdmin(){
        return this.user.role === 'admin';
    }

    isUser(){
        return this.user.role === 'user';
    }

    isAdminorUser(){
        if(this.user.role === 'admin' || this.user.role === 'user'){
            return true;
        }
    }
}