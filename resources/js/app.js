/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import 'vue2-dropzone/dist/vue2Dropzone.min.css'

import VueUploadComponent from 'vue-upload-component'
window.Vue = require('vue');
import moment from 'moment';

import VueRouter from 'vue-router'
Vue.use(VueRouter)
import { Form, HasError, AlertError } from 'vform';

import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);

window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)


import Datepicker from 'vuejs-datepicker';
Vue.component('datepicker', Datepicker)


import Multiselect from 'vue-multiselect';
Vue.component('multiselect', Multiselect)

Vue.component('pagination', require('laravel-vue-pagination'))

import QrcodeVue from 'qrcode.vue'
Vue.component('qrcode-vue', QrcodeVue)

import Swal from 'sweetalert2'
window.Swal = Swal;

const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer:3000
})

window.toast = toast;

Vue.filter('upText', function(text){
    return text.charAt(0).toUpperCase() + text.slice(1);
});
  
Vue.filter('myDate', function(created){
    moment.locale('id');
    return moment(created).format('LL');
});

Vue.filter('myDateTime', function(created){
    moment.locale('id');
    return moment(created).format('LLLL');
});

import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar,{
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
})



import downloadExcel from 'vue-json-excel'
Vue.component('downloadexcel', downloadExcel)

window.Fire = new Vue();


let routes = [
    { path: '/', component: require('./components/Dashboard.vue').default, meta: {title: 'Dashboard'} },
    { path: '/dashboard', component: require('./components/Dashboard.vue').default, meta: {title: 'Dashboard'} },
    { path: '/profile', component: require('./components/Profile.vue').default, meta: {title: 'Profil Pengguna'} },
    { path: '/kantor', component: require('./components/Office.vue').default, meta: {title: 'Kantor Cabang'} },
    { path: '/log', component: require('./components/Logs.vue').default, meta: {title: 'Aktivitas Log Sistem'} },
    { path: '/penggunasistem', component: require('./components/Users.vue').default, meta: {title: 'Pengguna Sistem'} },
    { path: '/developer', component: require('./components/Developer.vue').default, meta: {title: 'Developer Sistem'} },
    { path: '/referensinomorsurat', component: require('./components/ReferenceNumber.vue').default, meta: {title: 'Referensi Nomor Surat'} },
    { path: '/kategori', component: require('./components/Category.vue').default, meta: {title: 'Kategori Inventaris'} },
    { path: '/jenisbarang', component: require('./components/Type.vue').default, meta: {title: 'Jenis Barang'} },
    { path: '/proyek', component: require('./components/Project.vue').default, meta: {title: 'Proyek'} },
    { path: '/barang', component: require('./components/Itemss.vue').default, meta: {title: 'Data Barang'} },
    { path: '/formpeminjaman', name: 'formpeminjaman', component: require('./components/RentsItems.vue').default, meta: {title: 'Form Peminjaman Barang'}, props: true },
    { path: '/printpeminjaman', component: require('./components/PrintRentsPage.vue').default, meta: {title: 'Print Form Peminjaman Barang'}, props: true },
    { path: '/riwayatpeminjaman', component: require('./components/RentsHistory.vue').default, meta: {title: 'Riwayat Peminjaman Barang'} },
    { path: '/formpengembalian', name: 'formpengembalian', component: require('./components/ReturnItems.vue').default, meta: {title: 'Form Pengembalian Barang'}, props: true },
    { path: '/printpengembalian', component: require('./components/PrintReturnsPage.vue').default, meta: {title: 'Print Form Pengembalian'}, props: true },
    { path: '/riwayatpengembalian', component: require('./components/ReturnsHistory.vue').default, meta: {title: 'Riwayat Pengembalian Barang'} },
    { path: '/formperbaikan', name: 'formperbaikan', component: require('./components/ServiceItems.vue').default, meta: {title: 'Form Perbaikan Barang'}, props: true },,
    { path: '/printperbaikan', component: require('./components/PrintServicePage.vue').default, meta: {title: 'Print Form Perbaikan Barang'}, props: true },
    { path: '/riwayatperbaikan', component: require('./components/ServiceHistory.vue').default, meta: {title: 'Riwayat Perbaikan Barang'} },
    { path: '/formselesaiperbaikan', name: 'formselesaiperbaikan', component: require('./components/FixItems.vue').default, meta: {title: 'Form Selesai Perbaikan Barang'}, props: true },,
    { path: '/riwayatselesaiperbaikan', component: require('./components/FixHistory.vue').default, meta: {title: 'Riwayat Selesai Perbaikan Barang'} },
    { path: '/formpermintaanbarang', name: 'formpermintaanbarang', component: require('./components/RequestItems.vue').default, meta: {title: 'Form Permintaan Barang'}, props: true },,
    { path: '/riwayatpermintaan', component: require('./components/RequestHistory.vue').default, meta: {title: 'Riwayat Permintaan Barang'} },
    { path: '/formpengirimanbarang', name: 'formpengirimanbarang', component: require('./components/SendItems.vue').default, meta: {title: 'Form Surat Jalan Pengiriman Barang'}, props: true },,
    { path: '/printpengiriman', component: require('./components/PrintSendsPage.vue').default, meta: {title: 'Print Surat Jalan Pengiriman Barang'}, props: true },
    { path: '/riwayatpengiriman', component: require('./components/SendHistory.vue').default, meta: {title: 'Riwayat Pengiriman Barang'} },
    { path: '/rekapbarang', component: require('./components/ItemsRecap.vue').default, meta: {title: 'Rekap Data Barang'} },
    { path: '/riwayatpeminjamanuser', component: require('./components/HistoryRentsUser.vue').default, meta: {title: 'Rekap Riwayat Peminjaman User'} },
    { path: '/riwayatpeminjamanproyek', component: require('./components/HistoryItemsRentsByProject.vue').default, meta: {title: 'Rekap Data Peminjaman Proyek'} },
    { path: '/riwayatbarang', component: require('./components/ItemsRecapHistory.vue').default, meta: {title: 'Data Riwayat Barang'} },
    { path: '*', component: require('./components/NotFound.vue').default, meta: {title: 'SIM Inventory'} }

]

const router = new VueRouter({
    mode: 'history',
    routes
})


router.beforeEach((to,from, next)=> {
    document.title = to.meta.title
    // document.title = to.meta.title ? to.meta.title : 'Sim Inventory';
    next()
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component(
    'not-found',
    require('./components/NotFound.vue').default
);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    data :{
        search: ''
    },
    methods:{
        searchit: _.debounce(() => {
            Fire.$emit('searching');
        },1000)
    }
});
