<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendDetail extends Model
{
    protected $table = 'detail_pengiriman_barang';
    protected $fillable = ['nomor_surat','kode_barang','updated_by','deleted_by'];
    protected $primaryKey = 'detail_id';

    public function kantor(){
        return $this->belongsTo('App\Office');
    }

    public function barang(){
        return $this->hasMany('App\Items');
    }
}
