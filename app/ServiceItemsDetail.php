<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceItemsDetail extends Model
{
    protected $table = 'detail_perbaikan_barang';
    protected $fillable = ['nomor_surat','kode_barang','updated_by','deleted_by'];
    protected $primaryKey = 'detail_id';

    public function barang(){
        return $this->hasMany('App\Barang');
    }
}
