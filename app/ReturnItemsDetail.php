<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnItemsDetail extends Model
{
    protected $table = 'detail_pengembalian_barang';
    protected $fillable = ['nomor_surat','kode_barang','updated_by','deleted_by'];
    protected $primaryKey = 'detail_id';

    public function proyek(){
        return $this->belongsTo('App\Proyek');
    }

    public function barang(){
        return $this->hasMany('App\Barang');
    }
}
