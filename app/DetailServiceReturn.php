<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailServiceReturn extends Model
{
    protected $table = 'detail_barang_service_kembali';
    protected $fillable = ['barang_service_id','kode_barang','status','file_bukti','created_by','updated_by','deleted_by'];
    protected $primaryKey = 'detail_id';


    public function barang(){
        return $this->hasOne('App\Barang');
    }
}
