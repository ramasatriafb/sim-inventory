<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeaderRequestItems extends Model
{
    protected $table = 'header_permintaan_barang';
    protected $fillable = ['nomor_surat','tgl_permintaan','admin_cabang','keterangan','file_bukti','status','validasi',
    'updated_by'];
    protected $primaryKey = 'nomor_surat';

    protected $cast = ["tgl_permintaan"=> "DATE"];
}
