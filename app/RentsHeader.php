<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RentsHeader extends Model
{
    protected $table = 'header_peminjaman_barang';
    protected $fillable = ['nomor_surat','tgl_surat','proyek_id','keterangan','penanggung_jawab','peminjam','aktif','file_bukti',
    'updated_by','deleted_by'];
    protected $primaryKey = 'nomor_surat';

    protected $cast = ["tgl_surat"=> "DATE"];

    public function proyek(){
        return $this->belongsTo('App\Project');
    }

}
