<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsHistoryLog extends Model
{
    protected $table = 'riwayat_barang';
    protected $fillable = ['riwayat','user_id','tanggal','kode_barang'];
}
