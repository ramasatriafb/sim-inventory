<?php


namespace App\Helpers;
use Request;
use App\LogActivity as LogActivityModel;

use Illuminate\Support\Facades\DB;

class LogActivity
{


    public static function addToLog($subject)
    {
    	
    	LogActivityModel::create([
            'subject' => $subject,
           'url' => Request::fullUrl(),
           'method' => Request::method(),
           'ip' => Request::ip(),
           'agent' => Request::header('user-agent'),
           'user_id' => auth('api')->check() ? auth()->user()->id :1 ,
        ]);
    }


    public static function logActivityLists()
    {
        return DB::table('log_activity')->
        select('log_activity.id','subject', 'url', 'method','ip','agent',
        'users.name as name','log_activity.created_at')
            ->leftJoin(
            'users',
            'log_activity.user_id', '=', 'users.id'
            )
            ->orderBy('log_activity.created_at','desc')->paginate(10);
    }

    public static function search()
    {
        if ($search = \Request::get('q')){
            return DB::table('log_activity')->
            select('log_activity.id','subject', 'url', 'method','ip','agent',
            'users.name as name','log_activity.created_at')
                ->leftJoin(
                'users',
                'log_activity.user_id', '=', 'users.id'
                )->where(function($query) use ($search){
                    $query->where('log_activity.subject','LIKE', "%$search%");
                })
                ->orWhere(function($query) use ($search){
                    $query->where('users.name','LIKE', "%$search%");
                })
                ->orderBy('log_activity.created_at','desc')->paginate(10);
            }
    }

    public static function filter($tgl_awal,$tgl_akhir)
    {

        if($tgl_awal != 'all' && $tgl_awal != 'all' ) {
            return DB::table('log_activity')->
            select('log_activity.id','subject', 'url', 'method','ip','agent',
            'users.name as name','log_activity.created_at')
                ->leftJoin(
                'users',
                'log_activity.user_id', '=', 'users.id'
                )->whereBetween('log_activity.created_at', [$tgl_awal, $tgl_akhir])
                ->orderBy('log_activity.created_at','desc')->paginate(10);
           }else{
            return DB::table('log_activity')->
            select('log_activity.id','subject', 'url', 'method','ip','agent',
            'users.name as name','log_activity.created_at')
                ->leftJoin(
                'users',
                'log_activity.user_id', '=', 'users.id'
                )->orderBy('log_activity.created_at','desc')->paginate(10);
        }
    }


}