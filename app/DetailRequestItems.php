<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailRequestItems extends Model
{
    protected $table = 'detail_permintaan_barang';
    protected $fillable = ['nomor_surat','kode_barang','created_by','updated_by'];
    protected $primaryKey = 'detail_id';


    public function barang(){
        return $this->hasMany('App\Barang');
    }
}
