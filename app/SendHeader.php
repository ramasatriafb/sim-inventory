<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendHeader extends Model
{
    protected $table = 'header_pengiriman_barang';
    protected $fillable = ['nomor_surat','tgl_surat','kantor_id','keterangan','status','penerima','pengirim','permintaan_id','file_bukti','validasi','tgl_terima',
    'updated_by','deleted_by'];
    protected $primaryKey = 'nomor_surat';

    protected $cast = ["tgl_surat"=> "DATE"];

    public function proyek(){
        return $this->belongsTo('App\Project');
    }
}
