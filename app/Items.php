<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = 'barang';
    protected $fillable = ['kode_barang','nama_barang','merk','kelengkapan','status','foto_barang','file_invoice','lokasi_barang',
    'keterangan','updated_by','deleted_by','kategori_id', 'jenisbarang_id'];
    protected $primaryKey = 'kode_barang';

    public function kategori(){
        return $this->belongsTo('App\Kategori');
    }

    public function jenisbarang(){
        return $this->belongsTo('App\JenisBarang');
    }
}
