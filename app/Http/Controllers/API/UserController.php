<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('isAdmin')){
            return User::latest()->paginate(5);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (\Gate::allows('isAdmin')){
            $this->validate($request,[
                'name' => 'required|string|max:191',
                'email' => 'required|string|email|max:191|unique:users',
                'password' => 'required|string|min:8',
                'jabatan' => 'required|string|max:50',
                'no_telp' => 'required|numeric|min:8'
            ]);
    
            \LogActivity::addToLog($request->get('name').' Berhasil ditambahkan ke tabel User.');

            return User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'role' => $request['role'],
                'kantor_id' => $request['kantor_id'],
                'jabatan' => $request['jabatan'],
                'no_telp' => $request['no_telp'],
                'password' => Hash::make($request['password']),
            ]);
        }
    }

    public function updateProfile(Request $request)
    {
        $user =  auth('api')->user();

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|required|min:8',
            'jabatan' => 'required|string|max:50',
            'no_telp' => 'required|numeric|min:8'
        ]);

        $currentPhoto = $user->photo;
        if($request->photo != $currentPhoto){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('img/profile/').$name);

            $request->merge(['photo' => $name]);

            $userPhoto = public_path('img/profile/').$currentPhoto; 
            if(file_exists($userPhoto)){
                @unlink($userPhoto);
            }
        }

        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request['password'])]);
        }

        $user->update($request->all());
        return['message'=> "Success"];
    }

     public function profile()
    {
        return auth('api')->user();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (\Gate::allows('isAdmin')){
            $user = User::findOrFail($id);

            $this->validate($request,[
                'name' => 'required|string|max:191',
                'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
                'password' => 'sometimes|string|min:8',
                'jabatan' => 'required|string|max:50',
                'no_telp' => 'required|numeric|min:8'
            ]);
            $user->password = Hash::make($request['password']);

            \LogActivity::addToLog('Data '.$request->get('name').' Berhasil diperbarui ke tabel User.');

            $user->update([
                'name' => $request['name'],
                'email' => $request['email'],
                'role' => $request['role'],
                'kantor_id' => $request['kantor_id'],
                'jabatan' => $request['jabatan'],
                'no_telp' => $request['no_telp'],
                'password' => Hash::make($request['password']),
            ]);
            return ['message' => ' User Updated'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (\Gate::allows('isAdmin')){
            $user = User::findOrFail($id);

            \LogActivity::addToLog('Data '.$user->name.' Berhasil dihapus dari tabel User.');

            //delete user
            $user->delete();

            return ['message' => ' User Deleted'];
        }
    }

    public function search(){
        if ($search = \Request::get('q')){
            $users = User::where(function($query) use ($search){
                $query->where('name','LIKE', "%$search%");
            })->paginate(5);
        }else{
            $users = User::latest()->paginate(5);
        }
        return $users;
    }

    // Get All User
    public function getUser(){
        $users =  User::select('id','name')->get();
        $userData = [];
        foreach($users as $user){
            $userData[] = $user;
        }

        return $userData;
    }

    // Get Admin User
    public function getUserAdmin(){
        $users =  User::select('id','name')->whereNotNull('kantor_id')->get();
        $userData = [];
        foreach($users as $user){
            $userData[] = $user;
        }

        return $userData;
    }

    //  Get Admin Location
    public function getaAdminLocation(){
        $user =  auth('api')->user();

        $users = DB::table('users')->select('users.id', 'users.kantor_id', 'kantor.kota')
        ->leftJoin(
            'kantor',
            'kantor.id', '=', 'users.kantor_id'
        )->where('users.id','=',$user->id)->get();
        
        foreach ($users as $userr){
            $kantor = $userr->kantor_id;
        }
        
        return $kantor;
    }

    //  Get Admin Location Office
    public function getaAdminLocationOffice(){
        $user =  auth('api')->user();

        $users = DB::table('users')->select('users.id', 'users.kantor_id', 'kantor.kantor')
        ->leftJoin(
            'kantor',
            'kantor.id', '=', 'users.kantor_id'
        )->where('users.id','=',$user->id)->get();
        
        foreach ($users as $userr){
            $kantor = $userr->kantor;
        }
        
        return $kantor;
    }

    //  Get Admin ID
    public function getaUserID(){
        $user =  auth('api')->user();

        $users = DB::table('users')->select('users.id', 'users.kantor_id', 'kantor.kota')
        ->leftJoin(
            'kantor',
            'kantor.id', '=', 'users.kantor_id'
        )->where('users.id','=',$user->id)->get();
        
        foreach ($users as $userr){
            $id = $userr->id;
        }
        
        return $id;
    }

    
    
}
