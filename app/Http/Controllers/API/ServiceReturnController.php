<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\HeaderServiceReturn;
use App\DetailServiceReturn;
use App\Items;
use App\ItemsHistoryLog;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ServiceReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $user =  auth('api')->user();
        $firstDay = Carbon::now()->startOfMonth();
        $lastDay = Carbon::now()->endOfMonth();
        $firstDayConvert = date('Y-m-d', strtotime($firstDay));
        $lastDayConvert = date('Y-m-d', strtotime($lastDay));

        if (\Gate::allows('isAdmin')){
            $perbaikan = DB::table('header_barang_service_kembali')->
            select('header_barang_service_kembali.id as id','detail_barang_service_kembali.kode_barang','barang.nama_barang', 'header_barang_service_kembali.tgl_pengembalian',
            'penanggung_jawab.name as penanggung_jawab','pengembali_barang.name as pengembali_barang','header_barang_service_kembali.keterangan',
            'admin.name as admin','admin.id as admin_id', 'header_barang_service_kembali.created_at', 'detail_barang_service_kembali.file_bukti')
                ->join(
                'detail_barang_service_kembali',
                'header_barang_service_kembali.id', '=', 'detail_barang_service_kembali.barang_service_id'
                )->join(
                    'barang',
                    'detail_barang_service_kembali.kode_barang', '=', 'barang.kode_barang'
                )->join(
                    'users as pengembali_barang',
                    'header_barang_service_kembali.pengembali_barang', '=', 'pengembali_barang.id'
                )->join(
                    'users as penanggung_jawab',
                    'header_barang_service_kembali.penanggung_jawab', '=', 'penanggung_jawab.id'
                )->join(
                    'users as admin',
                    'header_barang_service_kembali.created_by', '=', 'admin.id'
                )->whereBetween('header_barang_service_kembali.tgl_pengembalian',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_barang_service_kembali.created_at','desc')->paginate(15);
            
            return $perbaikan;
        }else if (\ Gate::allows('isUser')){
            $perbaikan = DB::table('header_barang_service_kembali')->
            select('header_barang_service_kembali.id as id','detail_barang_service_kembali.kode_barang','barang.nama_barang', 'header_barang_service_kembali.tgl_pengembalian',
            'penanggung_jawab.name as penanggung_jawab','pengembali_barang.name as pengembali_barang','header_barang_service_kembali.keterangan',
            'admin.name as admin','admin.id as admin_id', 'header_barang_service_kembali.created_at', 'detail_barang_service_kembali.file_bukti')
                ->join(
                'detail_barang_service_kembali',
                'header_barang_service_kembali.id', '=', 'detail_barang_service_kembali.barang_service_id'
                )->join(
                    'barang',
                    'detail_barang_service_kembali.kode_barang', '=', 'barang.kode_barang'
                )->join(
                    'users as pengembali_barang',
                    'header_barang_service_kembali.pengembali_barang', '=', 'pengembali_barang.id'
                )->join(
                    'users as penanggung_jawab',
                    'header_barang_service_kembali.penanggung_jawab', '=', 'penanggung_jawab.id'
                )->join(
                    'users as admin',
                    'header_barang_service_kembali.created_by', '=', 'admin.id'
                )->where('header_barang_service_kembali.pengembali_barang','=', $user->id
                )->whereBetween('header_barang_service_kembali.tgl_pengembalian',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_barang_service_kembali.created_at','desc')->paginate(15);
            
            return $perbaikan;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();
        $rules = [
            'tgl_pengembalian' => 'required|string|max:191',
            'penanggung_jawab' => 'required',
            'pengembali_barang' => 'required',
            'file_bukti'=> 'required',
            'status'=> 'required',
            'barang'=>'required'
        ];
    
        $customMessages = [
            'barang.required' => 'Barang Harus Dipilih'
        ];
    
        $this->validate($request, $rules, $customMessages);
        $penanggung_jawab = $request->get('penanggung_jawab');
        $pengembali_barang = $request->get('pengembali_barang');
        $tgl_pengembalian_beforeFormat = $request->get('tgl_pengembalian');
        $tgl_pengembalian = date('Y-m-d', strtotime($tgl_pengembalian_beforeFormat));
        $keterangan = $request->get('keterangan');
        $status = $request->get('status');
        $barang = $request->get('barang');

        $header_pengembalian = new HeaderServiceReturn;
        $header_pengembalian->tgl_pengembalian = $tgl_pengembalian;
        $header_pengembalian->keterangan = $keterangan;
        $header_pengembalian->penanggung_jawab = $penanggung_jawab;
        $header_pengembalian->pengembali_barang = $pengembali_barang;
        $header_pengembalian->created_by = $user->id;
        $header_pengembalian->save();

        
        if ($request->file('file_bukti')) {
            $file = $request->file('file_bukti');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file = $request->file('file_bukti')->move(
                public_path('file/bukti-perbaikan-barang/'), $filename
            );
            
        }

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_pinjam){
            $detail_pengambalian = new DetailServiceReturn;
            $detail_pengambalian->barang_service_id =  $header_pengembalian->id;   
            $detail_pengambalian->kode_barang = $item{$no};
            $detail_pengambalian->status = $status;
            $detail_pengambalian->file_bukti = $filename;
            $detail_pengambalian->created_by = $user->id;
            $detail_pengambalian->save();
            \LogActivity::addToLog('Data '.$item{$no}.' Berhasil ditambahkan ke tabel Selesai Perbaikan Barang.');

            $barang_kembali = Items::findorFail($item{$no});
            $barang_kembali->dipinjam = 0;
            $barang_kembali->status = $status;
            $barang_kembali->update();
            

            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Perbaikan Barang dengan kode barang : '.$item{$no}.'. selesai pada tanggal :'. $tgl_pengembalian;
            $log->tanggal = $tgl_pengembalian;
            $log->user_id = $pengembali_barang;
            $log->save();

            $no++;
        }
        return ['message'=> "Success"];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');
        $header_pengembalian = HeaderServiceReturn::findorFail($id);
        $user =  auth('api')->user();
        $rules = [
            'tgl_pengembalian' => 'required|string|max:191',
            'penanggung_jawab' => 'required',
            'pengembali_barang' => 'required',
            'file_bukti'=> 'required',
            'status'=> 'required',
            'barang'=>'required'
        ];
    
    
        $customMessages = [
            'barang.required' => 'Barang Harus Dipilih'
        ];
    
        $this->validate($request, $rules, $customMessages);
        $penanggung_jawab = $request->get('penanggung_jawab');
        $pengembali_barang = $request->get('pengembali_barang');
        $tgl_pengembalian_beforeFormat = $request->get('tgl_pengembalian');
        $tgl_pengembalian = date('Y-m-d', strtotime($tgl_pengembalian_beforeFormat));
        $keterangan = $request->get('keterangan');
        $status = $request->get('status');
        $barang = $request->get('barang');

        $header_pengembalian->tgl_pengembalian = $tgl_pengembalian;
        $header_pengembalian->keterangan = $keterangan;
        $header_pengembalian->penanggung_jawab = $penanggung_jawab;
        $header_pengembalian->pengembali_barang = $pengembali_barang;
        $header_pengembalian->created_by = $user->id;
        $header_perbaikan->update();


        if ($request->file('file_bukti')) {
            $file = $request->file('file_bukti');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file = $request->file('file_bukti')->move(
                public_path('file/bukti-perbaikan-barang/'), $filename
            );
            
        }

        if ($request->file('file_bukti')) {
            $file = $request->file('file_bukti');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file = $request->file('file_bukti')->move(
                public_path('file/bukti-perbaikan-barang/'), $filename
            );
            
        }

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_perbaikan){
            $detail_pengembalian = new ServiceItemsDetail;    
            $detail_pengambalian->barang_service_id = $header_pengembalian->id;   
            $detail_pengambalian->kode_barang = $item{$no};
            $detail_pengambalian->status = $status;
            $detail_pengambalian->file_bukti = $filename;
            $detail_pengambalian->created_by = $user->id;
            $detail_pengembalian->save();
            \LogActivity::addToLog('Data '.$item{$no}.' Berhasil diperbarui ke tabel Selesai Perbaikan Barang.');

            $barang_perbaikan = Items::findorFail($item{$no});
            $barang_perbaikan->dipinjam = 0;
            $barang_perbaikan->status = $status;
            $barang_perbaikan->update();


            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Perbaikan Barang dengan kode barang : '.$item{$no}.'. selesai pada tanggal :'. $tgl_pengembalian;
            $log->tanggal = $tgl_pengembalian;
            $log->user_id = $pengembali_barang;
            $log->save();
            $no++;
        }
        return ['message'=> "Success"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();
        $headerItemsService = HeaderServiceReturn::findOrFail($id);
        $idDetail = $headerItemsService->id;
        $itemsServices = DetailServiceReturn::where('barang_service_id','=',$idDetail)->get();
        foreach ($itemsServices as $detail){
            $file_bukti = $detail->file_bukti;
            $kodebarang = $detail->kode_barang;
        }
        $currentFile = $file_bukti;

        // hapus file selesai perbaikan barang
       
        $itemFile = public_path('file/bukti-perbaikan-barang/').$currentFile; 
        if(file_exists($itemFile)){
            @unlink($itemFile);
        }

        
        
        $kode_barang = $kodebarang;
        $items = Items::findOrFail($kode_barang);
        $items->dipinjam = 2;
        $items->status = 'Perbaikan';
        $items->update();

        $tgl = Carbon::now();
        $tgl_batal = date('Y-m-d', strtotime($tgl));
        $log = new ItemsHistoryLog;
        $log->kode_barang = $kode_barang;
        $log->riwayat = 'Barang dengan kode barang : '.$kode_barang.'. batal selesai diperbaiki pada tanggal :'. $tgl_batal;
        $log->tanggal = $tgl_batal;
        $log->user_id = $user->id;
        $log->save();


        $detailServicesGet = DetailServiceReturn::where('barang_service_id','=',$idDetail)->first();
        $detailServicesGet->forceDelete();
        $headerItemsService->forceDelete();

        \LogActivity::addToLog('Data '.$kode_barang.' Berhasil dihapus dari tabel Selesai Perbaikan Barang.');


        return ['message' => ' Items Deleted'];
    }

    // get Header Selesai Perbaikan
    public function getHeaderSelesaiPerbaikan($id){
        $header_selesai_perbaikan = HeaderServiceReturn::findorFail($id);
        return $header_selesai_perbaikan;
    }

    // getDetail barang selesai perbaikan
    public function getDetailPerbaikanSelesai($id){
        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
        ->leftJoin(
            'kategori_inventaris',
            'kategori_inventaris.id', '=', 'barang.kategori_id'
        )->leftJoin(
            'jenis_barang',
            'jenis_barang.id' , '=', 'barang.jenisbarang_id'
        );
        $detail_perbaikan_selesai = DB::table('detail_barang_service_kembali')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_barang_service_kembali.kode_barang', '=', 'items.kode_barang');
        })->where('detail_barang_service_kembali.barang_service_id','=',$id)->get();
        return $detail_perbaikan_selesai;
    }

    // hapus barang selesai perbaikan
    public function deleteItemsSelesaiPerbaikan($id)
    {
        $this->authorize('isAdmin');

        $user =  auth('api')->user();
        $itemsServices = DetailServiceReturn::findOrFail($id);
        $currentFile = $itemsServices->file_bukti;

        // hapus file selesai perbaikan barang
       
        $itemFile = public_path('file/bukti-perbaikan-barang/').$currentFile; 
        if(file_exists($itemFile)){
            @unlink($itemFile);
        }
        
        $kodebarang = $itemsServices->kode_barang;
        $items = Items::findOrFail($kodebarang);
        $items->dipinjam = 2;
        $items->status = 'Perbaikan';
        $items->update();


        $tgl = Carbon::now();
        $tgl_batal = date('Y-m-d', strtotime($tgl));
        $log = new ItemsHistoryLog;
        $log->kode_barang = $kode_barang;
        $log->riwayat = 'Barang dengan kode barang : '.$kode_barang.'. batal selesai diperbaiki pada tanggal :'. $tgl_batal;
        $log->tanggal = $tgl_batal;
        $log->user_id = $user->id;
        $log->save();
        
        $itemsServices->forceDelete();

        return ['message' => ' Items Deleted'];
       
    }

    // Cara Data Barang Selesai Perbaikan

    public function search(){
        $user =  auth('api')->user();
        if (\Gate::allows('isAdmin') ){
            if ($search = \Request::get('q')){
                $perbaikan = DB::table('header_barang_service_kembali')->
                select('header_barang_service_kembali.id as id','detail_barang_service_kembali.kode_barang','barang.nama_barang', 'header_barang_service_kembali.tgl_pengembalian',
                'penanggung_jawab.name as penanggung_jawab','pengembali_barang.name as pengembali_barang','header_barang_service_kembali.keterangan',
                'admin.name as admin','admin.id as admin_id', 'header_barang_service_kembali.created_at', 'detail_barang_service_kembali.file_bukti')
                    ->join(
                    'detail_barang_service_kembali',
                    'header_barang_service_kembali.id', '=', 'detail_barang_service_kembali.barang_service_id'
                    )->join(
                        'barang',
                        'detail_barang_service_kembali.kode_barang', '=', 'barang.kode_barang'
                    )->join(
                        'users as pengembali_barang',
                        'header_barang_service_kembali.pengembali_barang', '=', 'pengembali_barang.id'
                    )->join(
                        'users as penanggung_jawab',
                        'header_barang_service_kembali.penanggung_jawab', '=', 'penanggung_jawab.id'
                    )->join(
                        'users as admin',
                        'header_barang_service_kembali.created_by', '=', 'admin.id'
                    )->where(function($query) use ($search){
                    $query->where('barang.nama_barang','LIKE', "%$search%");
                    })->orderBy('header_barang_service_kembali.created_at','desc')->paginate(15);
            }else{
                $perbaikan = DB::table('header_barang_service_kembali')->
                select('header_barang_service_kembali.id as id','detail_barang_service_kembali.kode_barang','barang.nama_barang', 'header_barang_service_kembali.tgl_pengembalian',
                'penanggung_jawab.name as penanggung_jawab','pengembali_barang.name as pengembali_barang','header_barang_service_kembali.keterangan',
                'admin.name as admin','admin.id as admin_id', 'header_barang_service_kembali.created_at', 'detail_barang_service_kembali.file_bukti')
                ->join(
                'detail_barang_service_kembali',
                'header_barang_service_kembali.id', '=', 'detail_barang_service_kembali.barang_service_id'
                )->join(
                    'barang',
                    'detail_barang_service_kembali.kode_barang', '=', 'barang.kode_barang'
                )->join(
                    'users as pengembali_barang',
                    'header_barang_service_kembali.pengembali_barang', '=', 'pengembali_barang.id'
                )->join(
                    'users as penanggung_jawab',
                    'header_barang_service_kembali.penanggung_jawab', '=', 'penanggung_jawab.id'
                )->join(
                    'users as admin',
                    'header_barang_service_kembali.created_by', '=', 'admin.id'
                )->orderBy('header_barang_service_kembali.created_by','desc')->paginate(15);
            }
            return $perbaikan;
        }else if (\Gate::allows('isUser') ){
            if ($search = \Request::get('q')){
                    $perbaikan = DB::table('header_barang_service_kembali')->
                    select('header_barang_service_kembali.id as id','detail_barang_service_kembali.kode_barang','barang.nama_barang', 'header_barang_service_kembali.tgl_pengembalian',
                    'penanggung_jawab.name as penanggung_jawab','pengembali_barang.name as pengembali_barang','header_barang_service_kembali.keterangan',
                    'admin.name as admin','admin.id as admin_id', 'header_barang_service_kembali.created_at', 'detail_barang_service_kembali.file_bukti')
                    ->join(
                    'detail_barang_service_kembali',
                    'header_barang_service_kembali.id', '=', 'detail_barang_service_kembali.barang_service_id'
                    )->join(
                        'barang',
                        'detail_barang_service_kembali.kode_barang', '=', 'barang.kode_barang'
                    )->join(
                        'users as pengembali_barang',
                        'header_barang_service_kembali.pengembali_barang', '=', 'pengembali_barang.id'
                    )->join(
                        'users as penanggung_jawab',
                        'header_barang_service_kembali.penanggung_jawab', '=', 'penanggung_jawab.id'
                    )->join(
                        'users as admin',
                        'header_barang_service_kembali.created_by', '=', 'admin.id'
                    )->where('header_barang_service_kembali.pengembali_barang', '=', $user->id)
                    ->where(function($query) use ($search){
                        $query->where('barang.nama_barang','LIKE', "%$search%");
                        })->orderBy('header_barang_service_kembali.created_at','desc')->paginate(15);
                }else{
                    $perbaikan = DB::table('header_barang_service_kembali')->
                    select('header_barang_service_kembali.id as id','detail_barang_service_kembali.kode_barang','barang.nama_barang', 'header_barang_service_kembali.tgl_pengembalian',
                    'penanggung_jawab.name as penanggung_jawab','pengembali_barang.name as pengembali_barang','header_barang_service_kembali.keterangan',
                    'admin.name as admin','admin.id as admin_id', 'header_barang_service_kembali.created_at', 'detail_barang_service_kembali.file_bukti')
                    ->join(
                    'detail_barang_service_kembali',
                    'header_barang_service_kembali.id', '=', 'detail_barang_service_kembali.barang_service_id'
                    )->join(
                        'barang',
                        'detail_barang_service_kembali.kode_barang', '=', 'barang.kode_barang'
                    )->join(
                        'users as pengembali_barang',
                        'header_barang_service_kembali.pengembali_barang', '=', 'pengembali_barang.id'
                    )->join(
                        'users as penanggung_jawab',
                        'header_barang_service_kembali.penanggung_jawab', '=', 'penanggung_jawab.id'
                    )->join(
                        'users as admin',
                        'header_barang_service_kembali.created_by', '=', 'admin.id'
                    )->where('header_barang_service_kembali.pengembali_barang', '=', $user->id)
                        ->orderBy('header_barang_service_kembali.created_by','desc')->paginate(15);
                    }
                return $perbaikan;
        }
    }

    // Filter Range Tanggal Selesai Perbaikan
    public function filter($tgl_awal, $tgl_akhir){
        $user =  auth('api')->user();
        if (\Gate::allows('isAdmin')){
        
            $perbaikan = DB::table('header_barang_service_kembali')->
                select('header_barang_service_kembali.id as id','detail_barang_service_kembali.kode_barang','barang.nama_barang', 'header_barang_service_kembali.tgl_pengembalian',
                'penanggung_jawab.name as penanggung_jawab','pengembali_barang.name as pengembali_barang','header_barang_service_kembali.keterangan',
                'admin.name as admin','admin.id as admin_id', 'header_barang_service_kembali.created_at', 'detail_barang_service_kembali.file_bukti')
                ->join(
                'detail_barang_service_kembali',
                'header_barang_service_kembali.id', '=', 'detail_barang_service_kembali.barang_service_id'
                )->join(
                    'barang',
                    'detail_barang_service_kembali.kode_barang', '=', 'barang.kode_barang'
                )->join(
                    'users as pengembali_barang',
                    'header_barang_service_kembali.pengembali_barang', '=', 'pengembali_barang.id'
                )->join(
                    'users as penanggung_jawab',
                    'header_barang_service_kembali.penanggung_jawab', '=', 'penanggung_jawab.id'
                )->join(
                    'users as admin',
                    'header_barang_service_kembali.created_by', '=', 'admin.id'
                )->whereBetween('header_barang_service_kembali.tgl_pengembalian',[$tgl_awal, $tgl_akhir])
                ->orderBy('header_barang_service_kembali.tgl_pengembalian','desc')
                ->paginate(15);
        
            return $perbaikan;
        } else  if (\Gate::allows('isUser')){
            $perbaikan = DB::table('header_barang_service_kembali')->
                select('header_barang_service_kembali.id as id','detail_barang_service_kembali.kode_barang','barang.nama_barang', 'header_barang_service_kembali.tgl_pengembalian',
                'penanggung_jawab.name as penanggung_jawab','pengembali_barang.name as pengembali_barang','header_barang_service_kembali.keterangan',
                'admin.name as admin','admin.id as admin_id', 'header_barang_service_kembali.created_at', 'detail_barang_service_kembali.file_bukti')
                ->join(
                'detail_barang_service_kembali',
                'header_barang_service_kembali.id', '=', 'detail_barang_service_kembali.barang_service_id'
                )->join(
                    'barang',
                    'detail_barang_service_kembali.kode_barang', '=', 'barang.kode_barang'
                )->join(
                    'users as pengembali_barang',
                    'header_barang_service_kembali.pengembali_barang', '=', 'pengembali_barang.id'
                )->join(
                    'users as penanggung_jawab',
                    'header_barang_service_kembali.penanggung_jawab', '=', 'penanggung_jawab.id'
                )->join(
                    'users as admin',
                    'header_barang_service_kembali.created_by', '=', 'admin.id'
                )->where('header_barang_service_kembali.pengembali_barang', '=', $user->id)
                ->whereBetween('header_barang_service_kembali.tgl_pengembalian',[$tgl_awal, $tgl_akhir])
                ->orderBy('header_barang_service_kembali.tgl_pengembalian','desc')
                ->paginate(15);

                return $perbaikan;
        }
    }
}
