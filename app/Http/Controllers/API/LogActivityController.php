<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    public function logActivity()
    {
        if (\Gate::allows('isAdmin') ){
            $logs = \LogActivity::logActivityLists();
            return $logs;
        }
    }

    public function search(){
        $logs = \LogActivity::search();
        return $logs;
    }

     // filter Recap Items
     public function filterLog($tgl_awal,$tgl_akhir){

        if (\Gate::allows('isAdmin')){
            $logs = \LogActivity::filter($tgl_awal,$tgl_akhir);
            return $logs;
        }
    }

}
