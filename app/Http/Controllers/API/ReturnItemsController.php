<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\ReturnItemsHeader;
use App\ReturnItemsDetail;
use App\Items;
use App\ItemsHistoryLog;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReturnItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $user =  auth('api')->user();
        $firstDay = Carbon::now()->startOfMonth();
        $lastDay = Carbon::now()->endOfMonth();
        $firstDayConvert = date('Y-m-d', strtotime($firstDay));
        $lastDayConvert = date('Y-m-d', strtotime($lastDay));

        if (\Gate::allows('isAdmin')){
            $pengembalian = DB::table('header_pengembalian_barang')->
            select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
            'admin.name as admin','admin.id as admin_id', 'header_pengembalian_barang.created_at', 'file_bukti')
                ->join(
                'proyek',
                'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                )->join(
                    'users as penanggungjawab',
                    'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pengembali',
                    'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                )->join(
                    'users as admin',
                    'header_pengembalian_barang.created_by', '=', 'admin.id'
                )->whereBetween('header_pengembalian_barang.tgl_pengembalian',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_pengembalian_barang.created_at','desc')->paginate(15);
                
            return $pengembalian;
        }else if ( \ Gate::allows('isUser')){
            $pengembalian = DB::table('header_pengembalian_barang')->
            select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
            'admin.name as admin','admin.id as admin_id', 'header_pengembalian_barang.created_at', 'file_bukti')
                ->join(
                'proyek',
                'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                )->join(
                    'users as penanggungjawab',
                    'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pengembali',
                    'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                )->join(
                    'users as admin',
                    'header_pengembalian_barang.created_by', '=', 'admin.id'
                )->where('header_pengembalian_barang.pengembali_barang', '=', $user->id)
                ->whereBetween('header_pengembalian_barang.tgl_pengembalian',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_pengembalian_barang.created_at','desc')->paginate(15);
                
            return $pengembalian;
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();

        $lokasi = $this->getUserLocation();

        $rules = [
            'nomor_surat' => 'required|string|max:191',
            'tgl_pengembalian' => 'required|string|max:191',
            'proyek_id' => 'required',
            'penanggung_jawab' => 'required',
            'pengembali_barang' => 'required',
            'keterangan'=> 'required',
            'barang'=>'required'
        ];
    
        $customMessages = [
            'barang.required' => 'Barang Harus Dipilih'
        ];
    
        $this->validate($request, $rules, $customMessages);
        
        $proyek = $request->get('proyek_id')['id'];
        $penanggung_jawab = $request->get('penanggung_jawab')['id'];
        $pengembali = $request->get('pengembali_barang')['id'];
        $tgl_pengembalian_beforeFormat = $request->get('tgl_pengembalian');
        $tgl_pengembalian = date('Y-m-d', strtotime($tgl_pengembalian_beforeFormat));
        $keterangan = $request->get('keterangan');
        $items = $request->get('barang');

        $header_pengembalian = new ReturnItemsHeader;
        $header_pengembalian->nomor_surat = $request->get('nomor_surat');
        $header_pengembalian->tgl_pengembalian = $tgl_pengembalian;
        $header_pengembalian->proyek_id = $proyek;
        $header_pengembalian->keterangan = $keterangan;
        $header_pengembalian->penanggung_jawab = $penanggung_jawab;
        $header_pengembalian->pengembali_barang = $pengembali;
        $header_pengembalian->aktif = 1;
        $header_pengembalian->created_by = $user->id;
        $header_pengembalian->save();
       
        $no =0;
        $item = (array) $items;
        foreach ($item as $items_return){
            $detail_pengembalian = new ReturnItemsDetail;    
            $detail_pengembalian->nomor_surat = $request->get('nomor_surat');
            $detail_pengembalian->kode_barang = $item{$no};
            $detail_pengembalian->lokasi_sebelumnya = $lokasi;
            $detail_pengembalian->created_by = $user->id;
            $detail_pengembalian->save();
            $barang_dikembalikan = Items::findorFail($item{$no});
            $barang_dikembalikan->dipinjam = 0;
            $lokasi_sebelumnya = $barang_dikembalikan->kantor_id;
            $barang_dikembalikan->kantor_id = $lokasi;
            $barang_dikembalikan->update();

            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Pengembalian Barang dengan kode barang : '.$item{$no}.'. pada tanggal :'. $tgl_pengembalian;
            $log->tanggal = $tgl_pengembalian;
            $log->user_id = $pengembali;
            $log->save();

            $no++;
        }

        $header_pengembalian->lokasi_sebelumnya = $lokasi_sebelumnya;
        \LogActivity::addToLog('Data '.$request->get('nomor_surat').' Berhasil ditambahkan ke tabel Surat Pengembalian Barang.');

        return ['message'=> "Success"];

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');
        $header_pengembalian = ReturnItemsHeader::findorFail($id);
        $user =  auth('api')->user();

        $lokasi = $this->getUserLocation();

        $rules = [
            'tgl_pengembalian' => 'required|string|max:191',
            'proyek_id' => 'required',
            'penanggung_jawab' => 'required',
            'pengembali_barang' => 'required',
            'keterangan'=> 'required',
        ];
    
    
        $this->validate($request, $rules);


        if(is_array( $request->get('proyek_id'))){
            $proyek = $request->get('proyek_id')['id'];
        
        }else{
            $proyek = $request->get('proyek_id');
        }

        if(is_array($request->get('penanggung_jawab'))){
            $penanggung_jawab = $request->get('penanggung_jawab')['id'];
        }else{
            $penanggung_jawab = $request->get('penanggung_jawab');
        }

        if(is_array(($request->get('pengembali_barang')))){
            $pengembali = $request->get('pengembali_barang')['id'];
        }else{
            $pengembali = $request->get('pengembali_barang');
        }

        $tgl_pengembalian_beforeFormat = $request->get('tgl_pengembalian');
        $tgl_pengembalian = date('Y-m-d', strtotime($tgl_pengembalian_beforeFormat));
        $keterangan = $request->get('keterangan');
        $items = $request->get('barang');

        $header_pengembalian->tgl_pengembalian = $tgl_pengembalian;
        $header_pengembalian->proyek_id = $proyek;
        $header_pengembalian->keterangan = $keterangan;
        $header_pengembalian->penanggung_jawab = $penanggung_jawab;
        $header_pengembalian->pengembali_barang = $pengembali;
        $header_pengembalian->aktif = 1;
        $header_pengembalian->updated_by = $user->id;
        $header_pengembalian->update();

        \LogActivity::addToLog('Data '.$id.' Berhasil diperbarui ke tabel Surat Pengembalian Barang.');

        $no =0;
        $item = (array) $items;
        foreach ($item as $barang_pinjam){
            $detail_pengembalian = new ReturnItemsDetail;    
            $detail_pengembalian->nomor_surat = $request->get('nomor_surat');
            $detail_pengembalian->kode_barang = $item{$no};
            $detail_pengembalian->lokasi_sebelumnya = $lokasi;
            $detail_pengembalian->created_by = $user->id;
            $detail_pengembalian->save();
            $barang_dikembalikan = Items::findorFail($item{$no});
            $barang_dikembalikan->dipinjam = 0;
            $barang_dikembalikan->kantor_id = $lokasi;
            $barang_dikembalikan->update();

            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Pengembalian Barang dengan kode barang : '.$item{$no}.'. pada tanggal :'. $tgl_pengembalian;
            $log->tanggal = $tgl_pengembalian;
            $log->user_id = $pengembali;
            $log->save();
            $no++;
        }
        return ['message'=> "Success"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');

        $user =  auth('api')->user();
        $itemsReturns = ReturnItemsHeader::findOrFail($id);
        $lokasi_sebelumnya = $itemsReturns->lokasi_sebelumnya;
        
        $fileName =  public_path('file/bukti-pengembaliann/').$itemsReturns->file_bukti;
        if(file_exists($fileName)){
            @unlink($fileName);
        }

        $detailReturns = ReturnItemsDetail::where('nomor_surat','=',$id)->get();

        $detailReturnsGet = ReturnItemsDetail::where('nomor_surat','=',$id)->first();

        foreach ($detailReturns as $detail){
           $items[] = $detail->kode_barang;
        }

        foreach ($items as $item){
            $itemsRents = Items::findOrFail($item);
            $itemsRents->dipinjam = 1;
            $items->kantor_id = $lokasi_sebelumnya;
            $itemsRents->update();

            $tgl = Carbon::now();
            $tgl_batal = date('Y-m-d', strtotime($tgl));
            $log = new ItemsHistoryLog;
            $log->kode_barang = $item;
            $log->riwayat = 'Barang dengan kode barang : '.$item.'. batal kembali pada tanggal :'. $tgl_batal;
            $log->tanggal = $tgl_batal;
            $log->user_id = $user->id;
            $log->save();

            $detailReturnsGet = ReturnItemsDetail::where('nomor_surat','=',$id)->first();
            $detailReturnsGet->forceDelete();
        }

        $itemsReturns->forceDelete();

        \LogActivity::addToLog('Data '.$id.' Berhasil dihapus dari tabel Surat Pengembalian Barang.');

        return ['message' => ' Return Items Deleted'];
       
    }

    public function getDataPengembalian($id){
        $header_pengembalian = DB::table('header_pengembalian_barang')->
        select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
         'admin.name as admin')
            ->join(
            'proyek',
            'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
            )->join(
                'users as penanggungjawab',
                'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
            )->join(
                'users as pengembali',
                'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
            )->join(
                'users as admin',
                'header_pengembalian_barang.created_by', '=', 'admin.id'
            )->where('nomor_surat','=',$id)->get();


        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_pengembalian = DB::table('detail_pengembalian_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_pengembalian_barang.kode_barang', '=', 'items.kode_barang');
        })->where('nomor_surat','=',$id)->get();

        $data_pengembalian = [
            'header_pengembalian'  => $header_pengembalian,
            'detail_pengembalian'   => $detail_pengembalian
        ];
        return $data_pengembalian;
    }

    public function getDetailPengembalian($id){
        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_pengembalian = DB::table('detail_pengembalian_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_pengembalian_barang.kode_barang', '=', 'items.kode_barang');
        })->where('detail_pengembalian_barang.nomor_surat','=',$id)->get();
        return $detail_pengembalian;
    }

    public function search(){
        $user =  auth('api')->user();
        if(\ Gate::allows('isAdmin')){
            if ($search = \Request::get('q')){
            $items = DB::table('header_pengembalian_barang')->
            select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
            'admin.name as admin','admin.id as admin_id','header_pengembalian_barang.created_at')
                ->join(
                'proyek',
                'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                )->join(
                    'users as penanggungjawab',
                    'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pengembali',
                    'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                )->join(
                    'users as admin',
                    'header_pengembalian_barang.created_by', '=', 'admin.id'
                )->where(function($query) use ($search){
                    $query->where('header_pengembalian_barang.nomor_surat','LIKE', "%$search%");
                    })->orderBy('header_pengembalian_barang.created_at','desc')->paginate(15);
            }else{
                $items =  DB::table('header_pengembalian_barang')->
                select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
                'admin.name as admin','admin.id as admin_id','header_pengembalian_barang.created_at')
                ->join(
                'proyek',
                'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                )->join(
                    'users as penanggungjawab',
                    'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pengembali',
                    'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                )->join(
                    'users as admin',
                    'header_pengembalian_barang.created_by', '=', 'admin.id'
                )->orderBy('header_pengembalian_barang.created_by','desc')->paginate(15);
            }
            return $items;
        } else if ( \ Gate::allows('isUser')){
            if ($search = \Request::get('q')){
                $items = DB::table('header_pengembalian_barang')->
                select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
                'admin.name as admin','admin.id as admin_id','header_pengembalian_barang.created_at')
                    ->join(
                    'proyek',
                    'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as pengembali',
                        'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                    )->join(
                        'users as admin',
                        'header_pengembalian_barang.created_by', '=', 'admin.id'
                    )->where('header_pengembalian_barang.pengembali_barang', '=', $user->id)
                    ->where(function($query) use ($search){
                        $query->where('header_pengembalian_barang.nomor_surat','LIKE', "%$search%");
                        })->orderBy('header_pengembalian_barang.created_at','desc')->paginate(15);
                }else{
                    $items =  DB::table('header_pengembalian_barang')->
                    select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
                    'admin.name as admin','admin.id as admin_id','header_pengembalian_barang.created_at')
                    ->join(
                    'proyek',
                    'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as pengembali',
                        'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                    )->join(
                        'users as admin',
                        'header_pengembalian_barang.created_by', '=', 'admin.id'
                    )->where('header_pengembalian_barang.pengembali_barang', '=', $user->id)
                    ->orderBy('header_pengembalian_barang.created_by','desc')->paginate(15);
                }
                return $items;
        }
    }
    
    public function filter($id_proyek, $tgl_awal, $tgl_akhir){
        $user =  auth('api')->user();
        if(\ Gate::allows('isAdmin')){
            if($id_proyek != 'all'){
                $pengembalian = DB::table('header_pengembalian_barang')->
                select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
                'admin.name as admin','admin.id as admin_id','header_pengembalian_barang.created_at', 'file_bukti')
                    ->join(
                    'proyek',
                    'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as pengembali',
                        'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                    )->join(
                        'users as admin',
                        'header_pengembalian_barang.created_by', '=', 'admin.id'
                    )->where('header_pengembalian_barang.proyek_id','=',$id_proyek
                    )->whereBetween('header_pengembalian_barang.tgl_pengembalian',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_pengembalian_barang.tgl_pengembalian','desc')
                    ->paginate(15);
            }else{
                $pengembalian = DB::table('header_pengembalian_barang')->
                select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
                'admin.name as admin','admin.id as admin_id','header_pengembalian_barang.created_at', 'file_bukti')
                    ->join(
                    'proyek',
                    'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as pengembali',
                        'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                    )->join(
                        'users as admin',
                        'header_pengembalian_barang.created_by', '=', 'admin.id'
                    )->whereBetween('header_pengembalian_barang.tgl_pengembalian',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_pengembalian_barang.tgl_pengembalian','desc')
                    ->paginate(15);
            }

            return $pengembalian;
        } else if (\ Gate::allows('isUser')){
            if($id_proyek != 'all'){
                $pengembalian = DB::table('header_pengembalian_barang')->
                select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
                'admin.name as admin','admin.id as admin_id','header_pengembalian_barang.created_at', 'file_bukti')
                    ->join(
                    'proyek',
                    'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as pengembali',
                        'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                    )->join(
                        'users as admin',
                        'header_pengembalian_barang.created_by', '=', 'admin.id'
                    )->where('header_pengembalian_barang.proyek_id','=',$id_proyek
                    )->where('header_pengembalian_barang.pengembali_barang', '=', $user->id)
                    ->whereBetween('header_pengembalian_barang.tgl_pengembalian',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_pengembalian_barang.tgl_pengembalian','desc')
                    ->paginate(15);
            }else{
                $pengembalian = DB::table('header_pengembalian_barang')->
                select('nomor_surat','tgl_pengembalian', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','pengembali.name as pengembali',
                'admin.name as admin','admin.id as admin_id','header_pengembalian_barang.created_at', 'file_bukti')
                    ->join(
                    'proyek',
                    'header_pengembalian_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_pengembalian_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as pengembali',
                        'header_pengembalian_barang.pengembali_barang', '=', 'pengembali.id'
                    )->join(
                        'users as admin',
                        'header_pengembalian_barang.created_by', '=', 'admin.id'
                    )->where('header_pengembalian_barang.pengembali_barang', '=', $user->id)
                    ->whereBetween('header_pengembalian_barang.tgl_pengembalian',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_pengembalian_barang.tgl_pengembalian','desc')
                    ->paginate(15);
            }

            return $pengembalian;
        }
    }

    public function uploadBukti(Request $request, $id)
    {
        $user =  auth('api')->user();

        $this->validate($request,[
            'file_bukti' => 'required'
        ]);

        $item = ReturnItemsHeader::findorFail($id);
        $currentFile = $item->file_bukti;
        if ($request->file('file_bukti')) {
            $file = $request->file('file_bukti');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file = $request->file('file_bukti')->move(
                public_path('file/bukti-pengembalian/'), $filename
            );


            $itemFile = public_path('file/bukti-pengembalian/').$currentFile; 
            if(file_exists($itemFile)){
                @unlink($itemFile);
            }
            
        }
       
       $item->update([
            'file_bukti' => $filename,
        ]);
        // return $item;
        return['message'=> "Success"];
    }

    public function deleteItemsReturns($id)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();
        $itemsReturns = ReturnItemsDetail::findOrFail($id);
        $kodebarang = $itemsReturns->kode_barang;
       $lokasi_sebelumnya = $itemsReturns->lokasi_sebelumnya;
        $items = Items::findOrFail($kodebarang);
        $items->dipinjam = 1;
        $items->kantor_id = $lokasi_sebelumnya;
        $items->update();

        $tgl = Carbon::now();
        $tgl_batal = date('Y-m-d', strtotime($tgl));

        $log = new ItemsHistoryLog;
        $log->kode_barang = $kodebarang;
        $log->riwayat = 'Barang dengan kode barang : '.$kodebarang.'. batal kembali pada tanggal :'. $tgl_batal;
        $log->tanggal = $tgl_batal;
        $log->user_id = $user->id;
        $log->save();

        $itemsReturns->forceDelete();

        return ['message' => ' Items Deleted'];
       
    }

    public function getHeaderPengembalian($id){
        $header_pengembalian = ReturnItemsHeader::findorFail($id);
        return $header_pengembalian->getAttributes('nomor_surat');
    }

    // User Location
    public function getUserLocation(){
        $user =  auth('api')->user();

        $users = DB::table('users')->select('users.id', 'users.kantor_id', 'kantor.kota')
        ->leftJoin(
            'kantor',
            'kantor.id', '=', 'users.kantor_id'
        )->where('users.id','=',$user->id)->get();
        
        foreach ($users as $userr){
            $kantor = $userr->kantor_id;
        }
        
        return $kantor;
    }
}
