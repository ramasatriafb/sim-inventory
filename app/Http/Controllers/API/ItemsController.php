<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Items;
use App\Category;
use App\Type;
use App\RecapItems;
use App\ItemsHistoryLog;
use App\RecapItemsHistoryLog;
use Illuminate\Support\Facades\DB;

use App\Exports\ItemsExport;
use Maatwebsite\Excel\Facades\Excel;

class ItemsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','kantor.id as kantor_id','kantor.kantor','barang.tgl_pembelian','barang.status' ,'barang.merk','barang.dipinjam','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'kantor',
                'kantor.id' , '=', 'barang.kantor_id'
            )->whereNull('deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
            return $items;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();

        $this->validate($request,[
            'kode_barang' => 'required|string|max:191|unique:barang',
            'nama_barang' => 'required|string|max:191',
            'foto_barang' => 'required',
            'kantor_id'     => 'required',
            'file_invoice' => 'required'
        ]);

        $tgl_pembelian_beforeFormat = $request->get('tgl_pembelian');
        $tgl_pembelian = date('Y-m-d', strtotime($tgl_pembelian_beforeFormat));
        
        $items = new Items;
        $items->kode_barang = $request->get('kode_barang');
        $items->nama_barang = $request->get('nama_barang');
        $items->merk = $request->get('merk');
        $items->kelengkapan = $request->get('kelengkapan');
        $items->status = $request->get('status');
        $items->tgl_pembelian = $request->get('tgl_pembelian');
        $foto_barang = $request->get('foto_barang');
        if($foto_barang){
            $foto = time().'.' . explode('/', explode(':', substr($request->foto_barang, 0, strpos($request->foto_barang, ';')))[1])[1];
            \Image::make($request->foto_barang)->save(public_path('img/items/').$foto);

            $items->foto_barang = $foto;
        }

        $file_invoice = $request->get('file_invoice');
        if($file_invoice){
            $invoice = time().'.' . explode('/', explode(':', substr($request->file_invoice, 0, strpos($request->file_invoice, ';')))[1])[1];
            \Image::make($request->file_invoice)->save(public_path('img/invoice-items/').$invoice);

            $items->file_invoice = $invoice;
        }
        $items->kantor_id = $request->get('kantor_id');
        $items->keterangan = $request->get('keterangan');
        $items->kategori_id = $request->get('kategori_id');
        $items->jenisbarang_id = $request->get('jenisbarang_id');
        $items->tgl_pembelian = $tgl_pembelian;
        
        $items->created_by = $user->id;
        $items->save();

        \LogActivity::addToLog('Data '.$request->get('kode_barang').'-'.$request->get('nama_barang').' Berhasil ditambahkan ke tabel Barang.');


        // $kodebarang = $request->get('kode_barang');
        // $nama_barang = $request->get('nama_barang');
        // $log = new ItemsHistoryLog;
        // $log->kode_barang = $kodebarang;
        // $log->riwayat = 'Menambahkan Barang dengan kode barang : '.$kodebarang.' nama barang : '. $nama_barang.' dengan tanggal pembelian :'. $tgl_pembelian;
        // $log->tanggal = $tgl_pembelian;
        // $log->user_id = $user->id;
        // $log->save();

        return['message'=> "Success"];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();

        $item = Items::findorFail($id);
        
        $this->validate($request,[
            'nama_barang' => 'required|string|max:191',
            'foto_barang' => 'required',
            'file_invoice' => 'required',
            'kantor_id' => 'required'
        ]);

        $tgl_pembelian_beforeFormat = $request->get('tgl_pembelian');
        $tgl_pembelian = date('Y-m-d', strtotime($tgl_pembelian_beforeFormat));
        $currentPhoto = $item->foto_barang;
        $foto_barang = $request->get('foto_barang');
        if($foto_barang != $currentPhoto){
            $nameFoto = time().'.' . explode('/', explode(':', substr($foto_barang, 0, strpos($foto_barang, ';')))[1])[1];
            \Image::make($foto_barang)->save(public_path('img/items/').$nameFoto);

            $request->merge(['foto_barang' => $nameFoto]);

            $itemPhoto = public_path('img/items/').$currentPhoto; 
            if(file_exists($itemPhoto)){
                @unlink($itemPhoto);
            }
        }

        $currentFile = $item->file_invoice;
        $file_invoice = $request->get('file_invoice');
        if($file_invoice != $currentFile){
            $nameFile = time().'.' . explode('/', explode(':', substr($file_invoice, 0, strpos($file_invoice, ';')))[1])[1];
            \Image::make($file_invoice)->save(public_path('img/invoice-items/').$nameFile);

            $request->merge(['file_invoice' => $nameFile]);

            $itemFile = public_path('img/invoice-items/').$currentFile; 
            if(file_exists($itemFile)){
                @unlink($itemFile);
            }
        }
        $item->kantor_id = $request->get('kantor_id');
        $item->keterangan = $request->get('keterangan');
        $item->kategori_id = $request->get('kategori_id');
        $item->jenisbarang_id = $request->get('jenisbarang_id');
        $item->tgl_pembelian = $tgl_pembelian;
        $item->updated_by = $user->id;
        $item->update($request->all());

        \LogActivity::addToLog('Data '.$id.'-'.$item->nama_barang.' Berhasil diperbarui ke tabel Barang.');

        // return $item;
        return['message'=> "Success"];    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kodebarang)
    {
        $this->authorize('isAdmin');
        $items = Items::findOrFail($kodebarang);
        if($items->dipinjam == 0){
            $items->forceDelete();

            \LogActivity::addToLog('Data '.$kodebarang.'-'.$items->nama_barang.' Berhasil dihapus dari tabel Barang.');

            return ['message' => ' Items Deleted'];
        }else{
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'error' => ['Barang sedang dipinjam tidak dapat di hapus'],
            ]);
            throw $error;
        }
    }

    //file trashed
    public function trash(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isAuthor')){
            $deleted_items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.lokasi_barang','barang.status' ,'barang.merk','barang.keterangan', 'barang.dipinjam','barang.deleted_at',
             'users.name AS nama')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->whereNotNull('deleted_at')->orderBy('barang.deleted_at','desc')->paginate(15);
            return $deleted_items;
        }
    }

    //restore items deleted
    public function restore($id){
        $items = Items::withTrashed()->findOrFail($id);
        if($items->trashed()){
        $items->restore();
        } else {
            return ['message' => 'Items is not in trash'];
        }
        return ['message' => 'Items successfully restored'];
    }

    // delete permanent
    public function deletePermanent($id){
        $items = Items::withTrashed()->findOrFail($id);
        if(!$items->trashed()){
            return ['message' => 'Can not delete permanent active items'];
        } else {
            $items->forceDelete();
            return ['message' => 'Items permanently deleted'];
        }
    }
    //find items
    public function search(){
        if ($search = \Request::get('q')){
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','kantor.id as kantor_id','kantor.kantor','barang.tgl_pembelian','barang.status' ,'barang.merk','barang.dipinjam','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'kantor',
                'kantor.id' , '=', 'barang.kantor_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where(function($query) use ($search){
                $query->where('barang.nama_barang','LIKE', "%$search%");
            })->whereNull('barang.deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.dipinjam','barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->whereNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->paginate(15);
        }
        return $items;
    }

    //find items deleted
    public function searchDeleted(){
        if ($search = \Request::get('q')){
           $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.deleted_at','users.name as nama')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where(function($query) use ($search){
                $query->where('barang.nama_barang','LIKE', "%$search%");
            })->whereNotNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->paginate(15);
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.deleted_at','users.name as nama')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->whereNotNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->paginate(15);
        }
        return $items;
    }

    //find items Available
    public function searchAvailable(){
        $lokasi = $this->getUserLocation();
        if ($search = \Request::get('q')){
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status','barang.merk','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.status','=','Baik')
            ->where('barang.dipinjam','=',0)
            ->where('barang.kantor_id','LIKE', "%$lokasi%")
            ->where(function($query) use ($search){
                $query->where('jenis_barang.jenis','LIKE', "%$search%");
            })
            ->whereNull('barang.deleted_at')->orderBy('barang.created_at','desc')->get();
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status','barang.merk','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.status','=','Baik')
            ->where('barang.dipinjam','=',0)
            ->where('barang.kantor_id','LIKE', "%$lokasi%")
            ->whereNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->get();
        }
        return $items;
    }

      //find items Borrowed
      public function searchBorrowed(){
        if ($search = \Request::get('q')){
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.status','=','Baik')
            ->where('barang.dipinjam','=',1)
            ->where(function($query) use ($search){
                $query->where('jenis_barang.jenis','LIKE', "%$search%");
            })
            ->whereNull('barang.deleted_at')->orderBy('barang.created_at','desc')->get();
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.status','=','Baik')
            ->where('barang.dipinjam','=',1)
            ->whereNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->get();
        }
        return $items;
    }

    //find items Service
    public function searchService(){
        $lokasi = $this->getUserLocation();
        if ($search = \Request::get('q')){
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori','barang.kelengkapan'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.dipinjam','=',2)
            ->where('barang.kantor_id','=',$lokasi)
            ->where(function($query) use ($search){
                $query->where('barang.kode_barang','LIKE', "%$search%");
            })
            ->whereNull('barang.deleted_at')->orderBy('barang.created_at','desc')->get();
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori','barang.kelengkapan'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.dipinjam','=',2)
            ->where('barang.kantor_id','=',$lokasi)
            ->whereNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->get();
        }
        return $items;
    }


    //filter items
    public function filter($kategori, $jenis, $status){
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
        , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
        ,'barang.foto_barang','barang.file_invoice','kantor.id as kantor_id','kantor.kantor','barang.tgl_pembelian','barang.status' ,'barang.merk','barang.dipinjam','barang.keterangan', 'barang.created_at')
        ->leftJoin(
            'kategori_inventaris',
            'kategori_inventaris.id', '=', 'barang.kategori_id'
        )->leftJoin(
            'jenis_barang',
            'jenis_barang.id' , '=', 'barang.jenisbarang_id'
        )->leftJoin(
            'kantor',
            'kantor.id' , '=', 'barang.kantor_id'
        )->when($kategori != 'all', function($items) use ($kategori){
            $items->where('kategori_inventaris.id', $kategori);
        })->when($jenis != 'all', function($items) use ($jenis){
            $items->where('jenis_barang.id', $jenis);
        })->when($status != 'all', function($items) use ($status){
            $items->where('barang.status', $status);
        })->whereNull('barang.deleted_at')
        ->orderBy('barang.created_at','desc')->paginate(15);

        return $items;
    }
    //filter items deleted
    public function filterDeleted($kategori, $jenis, $status){
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
        , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.deleted_at','users.name as nama')
        ->leftJoin(
            'kategori_inventaris',
            'kategori_inventaris.id', '=', 'barang.kategori_id'
        )->leftJoin(
            'jenis_barang',
            'jenis_barang.id' , '=', 'barang.jenisbarang_id'
        )->leftJoin(
            'users',
            'users.id' , '=', 'barang.deleted_by'
        )->whereNotNull('barang.deleted_at')
        ->when($kategori != 'all', function($items) use ($kategori){
            $items->where('kategori_inventaris.id', $kategori);
        })->when($jenis != 'all', function($items) use ($jenis){
            $items->where('jenis_barang.id', $jenis);
        })->when($status != 'all', function($items) use ($status){
            $items->where('barang.status', $status);
        })
        ->orderBy('barang.deleted_at','desc')->paginate(15);

        return $items;
    }

    public function getDataExcel(){
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
        , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
        ,'barang.foto_barang','barang.file_invoice','kantor.id as kantor_id','kantor.kantor','barang.tgl_pembelian','barang.status' ,'barang.merk','barang.dipinjam','barang.keterangan', 'barang.created_at')
        ->leftJoin(
            'kategori_inventaris',
            'kategori_inventaris.id', '=', 'barang.kategori_id'
        )->leftJoin(
            'jenis_barang',
            'jenis_barang.id' , '=', 'barang.jenisbarang_id'
        )->leftJoin(
            'kantor',
            'kantor.id' , '=', 'barang.kantor_id'
        )
            ->orderBy('barang.created_at','desc')->get();
        return $items;
    }

    // get Items Available
    public function getDataBarangAvailable($id = null){
        $lokasi = $this->getUserLocation();
        if ($id != null){
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status','barang.merk','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'kantor',
                'kantor.id' , '=', 'barang.kantor_id'
            )->where('status','=','Baik')->where('dipinjam','=',0)
            ->where('kantor_id','=', $id)
            ->whereNull('deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status','barang.merk','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'kantor',
                'kantor.id' , '=', 'barang.kantor_id'
            )->where('status','=','Baik')->where('dipinjam','=',0)
            ->where('kantor_id','=', $lokasi)
            ->whereNull('deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
        }
       
        return $items;
    }

    // Get Items Borrowed
    public function getDataBarangBorrowed(){
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status' ,'barang.merk','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->where('status','=','Baik')->where('dipinjam','=',1)
            ->whereNull('deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
            return $items;
    }

    //  Get Items Service
    public function getDataBarangService(){

        $lokasi = $this->getUserLocation();
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status' ,'barang.merk','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->where('dipinjam','=',2)
            ->where('barang.kantor_id','=',$lokasi)
            ->whereNull('deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
            return $items;
    }

    //  Get Items Broke
    public function getDataBarangRusak(){
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status' ,'barang.merk','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->where('dipinjam','=',0)
            ->where('status','=','Rusak')
            ->whereNull('deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
            return $items;
    }

     //find items searchBroke
     public function searchBroke(){
        if ($search = \Request::get('q')){
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.dipinjam','=',0)
            ->where('status','=','Rusak')
            ->where(function($query) use ($search){
                $query->where('barang.kode_barang','LIKE', "%$search%");
            })
            ->whereNull('barang.deleted_at')->orderBy('barang.created_at','desc')->get();
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.dipinjam','=',0)
            ->where('status','=','Rusak')
            ->whereNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->get();
        }
        return $items;
    }


    public function export_excel()
	{
		return Excel::download(new ItemsExport, 'barang.xlsx');
    }
    
    public function getDataDashboard(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            $items = Items::count();
            $available = Items::all()->where('dipinjam','=',0)->count();
            $rents = Items::all()->where('dipinjam','=',1)->count();
            $service = Items::all()->where('dipinjam','=',2)->count();
            $brokeItems = Items::all()->where('status', '=', 'Rusak')->count();
            $goodItems = Items::all()->where('status','=','Baik')->count();
            $repaireItems = Items::all()->where('status','=','Perbaikan')->count();
            $missItems = Items::all()->where('status','=','Hilang')->count();
            $category = Category::select('kategori')->get();

            

                
            $data_items = [
                'items'  => $items,
                'available'   => $available,
                'rents'  => $rents,
                'service' => $service,
                'brokeItems'   => $brokeItems,
                'goodItems' => $goodItems,
                'repaireItems' => $repaireItems,
                'missItems' => $missItems,
                'category' => $category
            ];

            return $data_items;
        }
    }

    public function getDataDashboardByOffice(){
        if (\Gate::allows('isAdmin')){
            $lokasi = $this->getUserLocation();

            $items = Items::where('kantor_id','=',$lokasi)->count();
            $available = Items::all()->where('dipinjam','=',0)->where('kantor_id','=',$lokasi)->count();
            $rents = Items::all()->where('dipinjam','=',1)->where('kantor_id','=',$lokasi)->count();
            $service = Items::all()->where('dipinjam','=',2)->where('kantor_id','=',$lokasi)->count();
            $brokeItems = Items::all()->where('status', '=', 'Rusak')->where('kantor_id','=',$lokasi)->count();
            $goodItems = Items::all()->where('status','=','Baik')->where('kantor_id','=',$lokasi)->count();
            $repaireItems = Items::all()->where('status','=','Perbaikan')->where('kantor_id','=',$lokasi)->count();
            $missItems = Items::all()->where('status','=','Hilang')->where('kantor_id','=',$lokasi)->count();
            $category = Category::select('kategori')->get();

            

                
            $data_items = [
                'items'  => $items,
                'available'   => $available,
                'rents'  => $rents,
                'service' => $service,
                'brokeItems'   => $brokeItems,
                'goodItems' => $goodItems,
                'repaireItems' => $repaireItems,
                'missItems' => $missItems,
                'category' => $category
            ];

            return $data_items;
        }
    }

    // Jumlah Barang berdsarkan Kategori
    public function statisticItems(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            $statisticItems = DB::table('barang')->selectRaw('kategori_inventaris.kategori, jenis_barang.jenis, COUNT(*) as total')
                ->leftJoin(
                    'kategori_inventaris',
                    'kategori_inventaris.id', '=', 'barang.kategori_id'
                )->leftJoin(
                    'jenis_barang',
                    'jenis_barang.id' , '=', 'barang.jenisbarang_id'
                )->groupBy('kategori_inventaris.kategori','jenis_barang.jenis')
                ->orderBy('kategori_inventaris.kategori','asc')->paginate(7);

            return $statisticItems;
        }
    }

    public function statisticItemsOffice(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            $statisticItems = DB::table('barang')->selectRaw('kantor.kantor, kategori_inventaris.kategori, jenis_barang.jenis, COUNT(*) as total')
                ->leftJoin(
                    'kategori_inventaris',
                    'kategori_inventaris.id', '=', 'barang.kategori_id'
                )->leftJoin(
                    'jenis_barang',
                    'jenis_barang.id' , '=', 'barang.jenisbarang_id'
                )->leftJoin(
                    'kantor',
                    'kantor.id' , '=', 'barang.kantor_id'
                )->groupBy('kantor.kantor','kategori_inventaris.kategori','jenis_barang.jenis')
                ->orderBy('kantor.kantor','asc')->paginate(7);

            return $statisticItems;
        }
    }

    // Jumlah Barang Tiap Kantor Cabang
    

    // Recap Items Views
    public function recapItems(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            return recapItems::orderBy('tgl_pembelian','desc')->paginate(35);
        }
    }

    // find Recap Items
    public function findRekapBarang(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            if ($search = \Request::get('q')){
                $items = DB::table('rekapbarangView')->select('kategori','jenis','kode_barang','nama_barang','merk','status'
                ,'ketersediaan','tgl_pembelian','peminjam','penanggung_jawab')
                ->where(function($query) use ($search){
                    $query->where('nama_barang','LIKE', "%$search%");
                })->orderBy('tgl_pembelian','desc')->paginate(35);
            }else{
                $items = recapItems::orderBy('tgl_pembelian','desc')->paginate(35);
            }
            return $items;
        }
    }

    // filter Recap Items
    public function filterRekapBarang($kategori, $jenis, $status, $tgl_awal,$tgl_akhir){

        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            if($kategori != 'all'){
                $category = Category::findOrFail($kategori);
                $kategori = $category->kategori;
            }
            if($jenis != 'all'){
                $type = Type::findOrFail($jenis);
                $jenis = $type->jenis;
            }

            if($tgl_awal != 'all' && $tgl_awal != 'all' ) {
                $items = DB::table('rekapbarangView')->select('kategori','jenis','kode_barang','nama_barang','merk','status'
                ,'ketersediaan','tgl_pembelian','peminjam','penanggung_jawab')
                ->when($kategori != 'all', function($items) use ($kategori){
                    $items->where('kategori', $kategori);
                })->when($jenis != 'all', function($items) use ($jenis){
                    $items->where('jenis', $jenis);
                })->when($status != 'all', function($items) use ($status){
                    $items->where('status', $status);
                })->whereBetween('tgl_pembelian',[$tgl_awal, $tgl_akhir])
                 ->orderBy('tgl_pembelian','desc')->paginate(35);
            }else{
                $items = DB::table('rekapbarangView')->select('kategori','jenis','kode_barang','nama_barang','merk','status'
                ,'ketersediaan','tgl_pembelian','peminjam','penanggung_jawab')
                ->when($kategori != 'all', function($items) use ($kategori){
                    $items->where('kategori', $kategori);
                })->when($jenis != 'all', function($items) use ($jenis){
                    $items->where('jenis', $jenis);
                })->when($status != 'all', function($items) use ($status){
                    $items->where('status', $status);
                })->orderBy('tgl_pembelian','desc')->paginate(35);
            }
            return $items;
        }
    }

    // get Data Excel Recap Items
    public function  getDataExcelRecap(){
        $items = DB::table('rekapbarangView')->select('kategori','jenis','kode_barang','nama_barang','merk','status'
        ,
        DB::raw('(CASE WHEN ketersediaan = 0 
        THEN "Tersedia" WHEN ketersediaan = 1 
        THEN "Sedang dipinjam" 
        ELSE "Sedang perbaikan" END) AS 
        ketersediaan')
        ,'tgl_pembelian','peminjam','penanggung_jawab')->get();
        return $items;
    }
    
     // Recap Items Views
     public function recapHistoryItems($id){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            $items = DB::table('rekapriwayatBarang')->select('kode_barang', 'riwayat', 'info_user', 'tanggal')
            ->where('rekapriwayatBarang.kode_barang','=',$id)->orderBy('tanggal','desc')->paginate(10);
        // dd($items) ;
            return $items;
        }
    }

    // User Location
    public function getUserLocation(){
        $user =  auth('api')->user();

        $users = DB::table('users')->select('users.id', 'users.kantor_id', 'kantor.kota')
        ->leftJoin(
            'kantor',
            'kantor.id', '=', 'users.kantor_id'
        )->where('users.id','=',$user->id)->get();
        
        foreach ($users as $userr){
            $kantor = $userr->kantor_id;
        }
        
        return $kantor;
    }
    
}