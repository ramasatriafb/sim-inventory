<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Items;
use App\HeaderRequestItems;
use App\DetailRequestItems;
use App\ItemsHistoryLog;
use App\Office;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RequestItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }


    public function index()
    {
        $user =  auth('api')->user();
        $firstDay = Carbon::now()->startOfMonth();
        $lastDay = Carbon::now()->endOfMonth();
        $firstDayConvert = date('Y-m-d', strtotime($firstDay));
        $lastDayConvert = date('Y-m-d', strtotime($lastDay));

        $permintaan = DB::table('header_permintaan_barang')->
        select('nomor_surat','tgl_permintaan','keterangan','kantor.kantor as kantor','kantor.id as kantor_id','status',
        'admin.name as admin', 'admin.id as admin_id', 'header_permintaan_barang.created_at')
            ->join(
                'kantor as kantor',
                'kantor.id', '=', 'header_permintaan_barang.kantor_id'
            )->join(
                'users as admin',
                'header_permintaan_barang.created_by', '=', 'admin.id'
            )
            ->whereBetween('header_permintaan_barang.tgl_permintaan',[$firstDayConvert, $lastDayConvert])
            ->orderBy('header_permintaan_barang.created_at','desc')->paginate(15);
        
        return $permintaan;
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();
        $rules = [
            'nomor_surat' => 'required|string|max:191',
            'tgl_permintaan' => 'required|string|max:191',
            'kantor_id' => 'required',
            'keterangan'=> 'required',
            'barang'=>'required'
        ];
    
        $customMessages = [
            'barang.required' => 'Barang Harus Dipilih'
        ];
    
        $this->validate($request, $rules, $customMessages);
        
        $kantor_id = $request->get('kantor_id');
        $tgl_permintaan_beforeFormat = $request->get('tgl_permintaan');
        $tgl_permintaan = date('Y-m-d', strtotime($tgl_permintaan_beforeFormat));
        $keterangan = $request->get('keterangan');
        $barang = $request->get('barang');

        $header_permintaan = new HeaderRequestItems;
        $header_permintaan->nomor_surat = $request->get('nomor_surat');
        $header_permintaan->tgl_permintaan = $tgl_permintaan;
        $header_permintaan->kantor_id = $kantor_id;
        $header_permintaan->keterangan = $keterangan;
        $header_permintaan->status = 'Dalam Proses';
        $header_permintaan->created_by = $user->id;
        $header_permintaan->save();

        $office = Office::findOrFail($kantor_id);
        $lokasi_kirim = $office->kantor;

        \LogActivity::addToLog('Data '.$request->get('nomor_surat').' Berhasil ditambahkan ke tabel Surat Permintaan Barang.');


        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_minta){
            $detail_permintaan = new DetailRequestItems;    
            $detail_permintaan->nomor_surat = $request->get('nomor_surat');
            $detail_permintaan->kode_barang = $item{$no};
            $detail_permintaan->created_by = $user->id;
            $detail_permintaan->save();
            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Permintaan Barang dengan kode barang : '.$item{$no}.'. ke '.$lokasi_kirim.' pada tanggal :'. $tgl_permintaan;
            $log->tanggal = $tgl_permintaan;
            $log->user_id = $user->id;
            $log->save();
            $no++;
        }
        return ['message'=> "Success"];

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');
        $header_permintaan = HeaderRequestItems::findorFail($id);
        $user =  auth('api')->user();
        $lokasi = $this->getUserLocation();
        $rules = [
            'nomor_surat' => 'required|string|max:191',
            'tgl_permintaan' => 'required|string|max:191',
            'kantor_id' => 'required',
            'keterangan'=> 'required',
        ];
    
    
        $this->validate($request, $rules);
        
        $kantor_id = $request->get('kantor_id');
        $tgl_permintaan_beforeFormat = $request->get('tgl_permintaan');
        $tgl_permintaan = date('Y-m-d', strtotime($tgl_permintaan_beforeFormat));
        $keterangan = $request->get('keterangan');
        $barang = $request->get('barang');


        $header_permintaan->nomor_surat = $request->get('nomor_surat');
        $header_permintaan->tgl_permintaan = $tgl_permintaan;
        $header_permintaan->kantor_id = $kantor_id;
        $header_permintaan->keterangan = $keterangan;
        $header_permintaan->status = 'Dalam Proses';
        $header_permintaan->created_by = $user->id;
        $header_permintaan->update();

        \LogActivity::addToLog('Data '.$id.' Berhasil diperbarui ke tabel Surat Permintaan Barang.');

        $office = Office::findOrFail($kantor_id);
        $lokasi_kirim = $office->kantor;

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_pinjam){
            $detail_permintaan = new DetailRequestItems;   
            $detail_permintaan->nomor_surat = $request->get('nomor_surat');
            $detail_permintaan->kode_barang = $item{$no};
            $detail_permintaan->created_by = $user->id;
            $detail_permintaan->save();
           
            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Melakukan Perubahan Permintaan Barang dengan kode barang : '.$item{$no}.'. ke '.$lokasi_kirim.' pada tanggal :'. $tgl_permintaan;
            $log->tanggal = $tgl_permintaan;
            $log->user_id = $user->id;
            $log->save();

            $no++;
        }
        return ['message'=> "Success"];
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');

        $user =  auth('api')->user();

        $itemsRequest = HeaderRequestItems::findOrFail($id);
        $kantor_id = $itemsRequest->kantor_id;

        $office = Office::findOrFail($kantor_id);
        $lokasi_kirim = $office->kantor;
        
        $detailRequest = DetailRequestItems::where('nomor_surat','=',$id)->get();
        $detailRequestGet = DetailRequestItems::where('nomor_surat','=',$id)->first();
       
        foreach ($detailRequest as $detail){
            $items[] = $detail->kode_barang;
        }

        foreach ($items as $item){
           
            $tgl = Carbon::now();
            $tgl_batal = date('Y-m-d', strtotime($tgl));

            $log = new ItemsHistoryLog;
            $log->kode_barang = $item;
            $log->riwayat = 'Barang dengan kode barang : '.$item.'. di '.$lokasi_kirim.' batal diminta pada tanggal :'. $tgl_batal;
            $log->tanggal = $tgl_batal;
            $log->user_id = $user->id;
            $log->save();
            
            $detailRequestGet = DetailRequestItems::where('nomor_surat','=',$id)->first();
            $detailRequestGet->forceDelete();
        }

        $itemsRequest->forceDelete();
        \LogActivity::addToLog('Data '.$id.' Berhasil dihapus dari tabel Surat Permintaan Barang.');

        return ['message' => ' Requrst Items Deleted'];
       
    }

    // Find REquest ITems

    public function search(){
        $user =  auth('api')->user();
        $firstDay = Carbon::now()->startOfMonth();
        $lastDay = Carbon::now()->endOfMonth();
        $firstDayConvert = date('Y-m-d', strtotime($firstDay));
        $lastDayConvert = date('Y-m-d', strtotime($lastDay));
        if ($search = \Request::get('q')){
            $permintaan = DB::table('header_permintaan_barang')->
            select('nomor_surat','tgl_permintaan','keterangan','kantor.kantor as kantor','kantor.id as kantor_id','status',
            'admin.name as admin','admin.id as admin_id', 'header_permintaan_barang.created_at')
                ->join(
                    'kantor as kantor',
                    'kantor.id', '=', 'header_permintaan_barang.kantor_id'
                )->join(
                    'users as admin',
                    'header_permintaan_barang.created_by', '=', 'admin.id'
                )->where(function($query) use ($search){
                    $query->where('header_permintaan_barang.nomor_surat','LIKE', "%$search%");
                })->orderBy('header_permintaan_barang.created_at','desc')->paginate(15);
            return $permintaan;
        }else{
            $permintaan = DB::table('header_permintaan_barang')->
            select('nomor_surat','tgl_permintaan','keterangan','kantor.kantor as kantor','kantor.id as kantor_id','status',
            'admin.name as admin','admin.id as admin_id', 'header_permintaan_barang.created_at')
                ->join(
                    'kantor as kantor',
                    'kantor.id', '=', 'header_permintaan_barang.kantor_id'
                )->join(
                    'users as admin',
                    'header_permintaan_barang.created_by', '=', 'admin.id'
                )->whereBetween('header_permintaan_barang.tgl_permintaan',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_permintaan_barang.created_at','desc')->paginate(15);
            
            return $permintaan;
        }
    }
    // Header Permintaan

    public function getHeaderPermintaan($id){

        $header_permintaan = HeaderRequestItems::findorFail($id);
        return $header_permintaan->getAttributes('nomor_surat');
    }

    // Detail Permintaan

    public function getDetailPermintaan($id){
        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_permintaan = DB::table('detail_permintaan_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_permintaan_barang.kode_barang', '=', 'items.kode_barang');
        })->where('detail_permintaan_barang.nomor_surat','=',$id)->get();
        return $detail_permintaan;
    }

    // Data Permintaan

    public function getDataPermintaan($id){
        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_permintaan = DB::table('detail_permintaan_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_permintaan_barang.kode_barang', '=', 'items.kode_barang');
        })->where('detail_permintaan_barang.nomor_surat','=',$id)->get();
        return $detail_permintaan;
    }

    // Filter By Date Create
    public function filter($tgl_awal, $tgl_akhir){
        $user =  auth('api')->user();
        
        $permintaan = DB::table('header_permintaan_barang')->
        select('nomor_surat','tgl_permintaan','keterangan','kantor.kantor as kantor','kantor.id as kantor_id','status',
        'admin.name as admin', 'header_permintaan_barang.created_at')
            ->join(
                'kantor as kantor',
                'kantor.id', '=', 'header_permintaan_barang.kantor_id'
            )->join(
                'users as admin',
                'header_permintaan_barang.created_by', '=', 'admin.id'
            )->whereBetween('header_permintaan_barang.tgl_permintaan',[$tgl_awal, $tgl_akhir])
            ->orderBy('header_permintaan_barang.created_at','desc')->paginate(15);
    
        return $permintaan;
    }

    // Delete Items Request
    public function deleteItemsRequest($id)
    {
        $this->authorize('isAdmin');

        $user =  auth('api')->user();

        $lokasi_kirim = DB::table('header_permintaan_barang')->
        select('nomor_surat','tgl_permintaan','keterangan','kantor.kantor as kantor','kantor.id as kantor_id','status',
        'admin.name as admin','admin.id as admin_id', 'header_permintaan_barang.created_at')
            ->join(
                'kantor as kantor',
                'kantor.id', '=', 'header_permintaan_barang.kantor_id'
            )->join(
                'users as admin',
                'header_permintaan_barang.created_by', '=', 'admin.id'
            )->where('header_permintaan_barang.nomor_surat','=',$id)->get();

        foreach ($lokasi_kirim as $lokasi_kirim){
            $kantor = $lokasi_kirim->kantor;
        }

        $itemsRequest = DetailRequestItems::findOrFail($id);
        $kodebarang = $itemsRequest->kode_barang;

        $tgl = Carbon::now();
        $tgl_batal = date('Y-m-d', strtotime($tgl));

        $log = new ItemsHistoryLog;
        $log->kode_barang = $kodebarang;
        $log->riwayat = 'Barang dengan kode barang : '.$kodebarang.'. di '.$lokasi_kirim.' batal diminta pada tanggal :'. $tgl_batal;
        $log->tanggal = $tgl_batal;
        $log->user_id = $user->id;
        $log->save();
        
        $itemsRequest->forceDelete();

        return ['message' => ' Items Deleted'];
       
    }

    public function validasiPermintaan(Request $request, $id)
    {
        $user =  auth('api')->user();

        $this->validate($request,[
            'validasi' => 'required'
        ]);

        $item = HeaderRequestItems::findorFail($id);
        
        $validasi =  $request->get('validasi');
        $valueValidasi ='';
        if($validasi == 'Disetujui'){
            $valueValidasi = 1;
        }else{
            $valueValidasi = 2;
        }
        $item->update([
            'status' => $validasi,
            'validasi' => $valueValidasi,
        ]);
        // return $item;
        return['message'=> "Success"];
    }

     // Get Nomor Surat Request

     public function getPermintaan(){
        $lokasi = $this->getUserLocation();
        $requests =  HeaderRequestItems::select('nomor_surat')->where('kantor_id','=',$lokasi)->where('status','Disetujui')->get();
        $requestData = [];
        foreach($requests as $request){
            $requestData[] = $request->getAttributes('nomor_surat');
        }

        return $requestData;
    }

    // User Location
    public function getUserLocation(){
        $user =  auth('api')->user();

        $users = DB::table('users')->select('users.id', 'users.kantor_id', 'kantor.kota')
        ->leftJoin(
            'kantor',
            'kantor.id', '=', 'users.kantor_id'
        )->where('users.id','=',$user->id)->get();
        
        foreach ($users as $userr){
            $kantor = $userr->kantor_id;
        }
        
        return $kantor;
    }
    
   
}
