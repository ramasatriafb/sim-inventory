<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\SendHeader;
use App\SendDetail;
use App\Items;
use App\ItemsHistoryLog;
use App\Office;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SendItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }


    public function index()
    {
        $user =  auth('api')->user();
        $firstDay = Carbon::now()->startOfMonth();
        $lastDay = Carbon::now()->endOfMonth();
        $firstDayConvert = date('Y-m-d', strtotime($firstDay));
        $lastDayConvert = date('Y-m-d', strtotime($lastDay));

        if (\Gate::allows('isAdmin') ){
            $peminjaman = DB::table('header_pengiriman_barang')->
            select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
            'admin.name as admin','admin.id as admin_id','tgl_terima', 'header_pengiriman_barang.created_at', 'file_bukti')
                ->join(
                'kantor',
                'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
                )->join(
                    'users as penerima',
                    'header_pengiriman_barang.penerima', '=', 'penerima.id'
                )->join(
                    'users as pengirim',
                    'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
                )->join(
                    'users as admin',
                    'header_pengiriman_barang.created_by', '=', 'admin.id'
                )->whereBetween('header_pengiriman_barang.tgl_surat',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_pengiriman_barang.created_at','desc')->paginate(15);
            
            return $peminjaman;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();

        $lokasi = $this->getUserLocation();
        $rules = [
            'nomor_surat' => 'required|string|max:191',
            'tgl_surat' => 'required|string|max:191',
            'kantor_id' => 'required',
            'penerima' => 'required',
            'pengirim' => 'required',
            'keterangan'=> 'required',
            'barang'=>'required'
        ];
    
        $customMessages = [
            'barang.required' => 'Barang Harus Dipilih'
        ];
    
        $this->validate($request, $rules, $customMessages);
        
        $kantor = $request->get('kantor_id')['id'];
        $penerima = $request->get('penerima')['id'];
        $pengirim = $request->get('pengirim')['id'];
        if($request->get('permintaan_id') !=''){
            $permintaan_id = $request->get('permintaan_id')['nomor_surat'];
        }else{
            $permintaan_id = $request->get('permintaan_id');
        }
       
        $tgl_kirim_beforeFormat = $request->get('tgl_surat');
        $tgl_kirim = date('Y-m-d', strtotime($tgl_kirim_beforeFormat));
        $keterangan = $request->get('keterangan');
        $barang = $request->get('barang');

        $header_pengiriman = new SendHeader;
        $header_pengiriman->nomor_surat = $request->get('nomor_surat');
        $header_pengiriman->tgl_surat = $tgl_kirim;
        $header_pengiriman->kantor_id = $kantor;
        $header_pengiriman->keterangan = 'Dalam Proses';
        $header_pengiriman->keterangan = $keterangan;
        $header_pengiriman->penerima = $penerima;
        $header_pengiriman->pengirim = $pengirim;
        $header_pengiriman->permintaan_id = $permintaan_id;
        $header_pengiriman->created_by = $user->id;
        $header_pengiriman->save();

        \LogActivity::addToLog('Data '.$request->get('nomor_surat').' Berhasil ditambahkan ke tabel Surat Pengiriman Barang.');

        $office = Office::findOrFail($kantor);
        $lokasi_kirim = $office->kantor;

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_kirim){
            $detail_pengiriman = new SendDetail;    
            $detail_pengiriman->nomor_surat = $request->get('nomor_surat');
            $detail_pengiriman->kode_barang = $item{$no};
            $detail_pengiriman->lokasi_sebelumnya = $lokasi;
            $detail_pengiriman->created_by = $user->id;
            $detail_pengiriman->save();
            $barang_kirim = Items::findorFail($item{$no});
            $barang_kirim->dipinjam = 3;
            $barang_kirim->update();
            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Pengiriman Barang dengan kode barang : '.$item{$no}.'. dari '.$lokasi.' dikirim ke '.$lokasi_kirim.' pada tanggal :'. $tgl_kirim;
            $log->tanggal = $tgl_kirim;
            $log->user_id = $pengirim;
            $log->save();
            $no++;
        }
        return ['message'=> "Success"];

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');
        $header_pengiriman = SendHeader::findorFail($id);
        $user =  auth('api')->user();
        $lokasi = $this->getUserLocation();
        $rules = [
            'tgl_surat' => 'required|string|max:191',
            'kantor_id' => 'required',
            'penerima' => 'required',
            'pengirim' => 'required',
            'keterangan'=> 'required',
        ];
    
    
        $this->validate($request, $rules);

        if(is_array( $request->get('kantor_id'))){
            $kantor = $request->get('kantor_id')['id'];
        
        }else{
            $kantor = $request->get('kantor_id');
        }

        if(is_array($request->get('penerima'))){
            $penerima = $request->get('penerima')['id'];
        }else{
            $penerima = $request->get('penerima');
        }

        if(is_array(($request->get('pengirim')))){
            $pengirim = $request->get('pengirim')['id'];
        }else{
            $pengirim = $request->get('pengirim');
        }


        $tgl_kirim_beforeFormat = $request->get('tgl_surat');
        $tgl_kirim = date('Y-m-d', strtotime($tgl_kirim_beforeFormat));
        $keterangan = $request->get('keterangan');
        $barang = $request->get('barang');

        $header_pengiriman->tgl_surat = $tgl_kirim;
        $header_pengiriman->kantor_id = $kantor;
        $header_pengiriman->keterangan = $keterangan;
        $header_pengiriman->penerima = $penerima;
        $header_pengiriman->pengirim = $pengirim;
        $header_pengiriman->updated_by = $user->id;
        $header_pengiriman->update();

        \LogActivity::addToLog('Data '.$id.' Berhasil diperbarui ke tabel Surat Pengiriman Barang.');


        $office = Office::findOrFail($kantor);
        $lokasi_kirim = $office->kantor;

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_kirim){
            $detail_pengiriman = new SendDetail;    
            $detail_pengiriman->nomor_surat = $request->get('nomor_surat');
            $detail_pengiriman->kode_barang = $item{$no};
            $detail_pengiriman->lokasi_sebelumnya = $lokasi;
            $detail_pengiriman->created_by = $user->id;
            $detail_pengiriman->save();
            $barang_kirim = Items::findorFail($item{$no});
            $barang_kirim->dipinjam = 3;
            $barang_kirim->update();
            
            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Pengiriman Barang dengan kode barang : '.$item{$no}.'dari '.$lokasi.'. dikirim ke '.$lokasi_kirim.' pada tanggal :'. $tgl_kirim;
            $log->tanggal = $tgl_kirim;
            $log->user_id = $pengirim;
            $log->save();

            $no++;
        }
        return ['message'=> "Success"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');

        $user =  auth('api')->user();

        $itemsSends = RentsHeader::findOrFail($id);
        $kantor = $itemsSends->kantor_id;

        $office = Office::findOrFail($kantor_id);
        $lokasi_kirim = $office->kantor;

        $fileName =  public_path('file/bukti-pengiriman/').$itemsSends->file_bukti;
        if(file_exists($fileName)){
            @unlink($fileName);
        }

        $detailSend = SendDetail::where('nomor_surat','=',$id)->get();
        $detailSendsGet = SendDetail::where('nomor_surat','=',$id)->first();
       
        foreach ($detailSend as $detail){
            $items[] = $detail->kode_barang;
        }

        foreach ($items as $item){
            $itemsRent = Items::findOrFail($item);
            $itemsRent->dipinjam = 0;
            $itemsRent->update();

            $tgl = Carbon::now();
            $tgl_batal = date('Y-m-d', strtotime($tgl));

            $log = new ItemsHistoryLog;
            $log->kode_barang = $item;
            $log->riwayat = 'Barang dengan kode barang : '.$item.'. batal Dikirm ke '.$lokasi_kirim.' pada tanggal :'. $tgl_batal;
            $log->tanggal = $tgl_batal;
            $log->user_id = $user->id;
            $log->save();
            
            $detailSendsGet = SendDetail::where('nomor_surat','=',$id)->first();
            $detailSendsGet->forceDelete();
        }

        $itemsSends->forceDelete();

        \LogActivity::addToLog('Data '.$id.' Berhasil dihapus dari tabel Surat Pengiriman Barang.');

        return ['message' => ' Rents Items Deleted'];
    }

    // Search Send Items
    public function search(){
        $user =  auth('api')->user();
        if (\Gate::allows('isAdmin') ){
            if ($search = \Request::get('q')){
            $items = DB::table('header_pengiriman_barang')->
            select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
            'admin.name as admin','admin.id as admin_id','tgl_terima', 'header_pengiriman_barang.created_at', 'file_bukti')
                ->join(
                'kantor',
                'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
                )->join(
                    'users as penerima',
                    'header_pengiriman_barang.penerima', '=', 'penerima.id'
                )->join(
                    'users as pengirim',
                    'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
                )->join(
                    'users as admin',
                    'header_pengiriman_barang.created_by', '=', 'admin.id'
                )->where(function($query) use ($search){
                    $query->where('header_pengiriman_barang.nomor_surat','LIKE', "%$search%");
                    })->orderBy('header_pengiriman_barang.created_at','desc')->paginate(15);
            }else{
                $items = DB::table('header_pengiriman_barang')->
                select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
                'admin.name as admin','admin.id as admin_id','tgl_terima', 'header_pengiriman_barang.created_at', 'file_bukti')
                    ->join(
                    'kantor',
                    'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
                    )->join(
                        'users as penerima',
                        'header_pengiriman_barang.penerima', '=', 'penerima.id'
                    )->join(
                        'users as pengirim',
                        'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
                    )->join(
                        'users as admin',
                        'header_pengiriman_barang.created_by', '=', 'admin.id'
                    )->orderBy('header_pengiriman_barang.created_by','desc')->paginate(15);
            }
            return $items;
        }else if (\Gate::allows('isUser') ){
            if ($search = \Request::get('q')){
                $items =  DB::table('header_pengiriman_barang')->
                select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
                'admin.name as admin','admin.id as admin_id','tgl_terima', 'header_pengiriman_barang.created_at', 'file_bukti')
                    ->join(
                    'kantor',
                    'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
                    )->join(
                        'users as penerima',
                        'header_pengiriman_barang.penerima', '=', 'penerima.id'
                    )->join(
                        'users as pengirim',
                        'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
                    )->join(
                        'users as admin',
                        'header_pengiriman_barang.created_by', '=', 'admin.id'
                    )->where('header_pengiriman_barang.peminjam', '=', $user->id)
                    ->where(function($query) use ($search){
                        $query->where('header_pengiriman_barang.nomor_surat','LIKE', "%$search%");
                        })->orderBy('header_pengiriman_barang.created_at','desc')->paginate(15);
                }else{
                    $items =  DB::table('header_pengiriman_barang')->
                    select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
                    'admin.name as admin','admin.id as admin_id','tgl_terima', 'header_pengiriman_barang.created_at', 'file_bukti')
                        ->join(
                        'kantor',
                        'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
                        )->join(
                            'users as penerima',
                            'header_pengiriman_barang.penerima', '=', 'penerima.id'
                        )->join(
                            'users as pengirim',
                            'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
                        )->join(
                            'users as admin',
                            'header_pengiriman_barang.created_by', '=', 'admin.id'
                        )->where('header_pengiriman_barang.peminjam', '=', $user->id)
                    ->orderBy('header_pengiriman_barang.created_by','desc')->paginate(15);
                }
                return $items;
        }
    }

    // Filter Send Items
    public function filter($id_kantor, $tgl_awal, $tgl_akhir){
        $user =  auth('api')->user();
        if (\Gate::allows('isAdmin') ){
            if($id_kantor != 'all'){
                $peminjaman =DB::table('header_pengiriman_barang')->
                select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
                'admin.name as admin','admin.id as admin_id','tgl_terima', 'header_pengiriman_barang.created_at', 'file_bukti')
                    ->join(
                    'kantor',
                    'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
                    )->join(
                        'users as penerima',
                        'header_pengiriman_barang.penerima', '=', 'penerima.id'
                    )->join(
                        'users as pengirim',
                        'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
                    )->join(
                        'users as admin',
                        'header_pengiriman_barang.created_by', '=', 'admin.id'
                    )->where('header_pengiriman_barang.kantor_id','=',$id_kantor
                    )->whereBetween('header_pengiriman_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_pengiriman_barang.tgl_surat','desc')
                    ->paginate(15);
            }else{
                $peminjaman = DB::table('header_pengiriman_barang')->
                select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
                'admin.name as admin','admin.id as admin_id','tgl_terima', 'header_pengiriman_barang.created_at', 'file_bukti')
                    ->join(
                    'kantor',
                    'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
                    )->join(
                        'users as penerima',
                        'header_pengiriman_barang.penerima', '=', 'penerima.id'
                    )->join(
                        'users as pengirim',
                        'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
                    )->join(
                        'users as admin',
                        'header_pengiriman_barang.created_by', '=', 'admin.id'
                    )->whereBetween('header_pengiriman_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_pengiriman_barang.tgl_surat','desc')
                    ->paginate(15);
            }
            return $peminjaman;
        } else if (\Gate::allows('isUser') ){
            if($id_kantor != 'all'){
                $pengiriman = DB::table('header_pengiriman_barang')->
                select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
                'admin.name as admin','admin.id as admin_id','tgl_terima', 'header_pengiriman_barang.created_at', 'file_bukti')
                    ->join(
                    'kantor',
                    'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
                    )->join(
                        'users as penerima',
                        'header_pengiriman_barang.penerima', '=', 'penerima.id'
                    )->join(
                        'users as pengirim',
                        'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
                    )->join(
                        'users as admin',
                        'header_pengiriman_barang.created_by', '=', 'admin.id'
                    )->where('header_pengiriman_barang.kantor_id','=',$id_kantor
                    )->where('header_pengiriman_barang.pengirim', '=', $user->id)
                    ->whereBetween('header_pengiriman_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_pengiriman_barang.tgl_surat','desc')
                    ->paginate(15);
            }else{
                $peminjaman = DB::table('header_pengiriman_barang')->
                select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
                'admin.name as admin','admin.id as admin_id','tgl_terima', 'header_pengiriman_barang.created_at', 'file_bukti')
                    ->join(
                    'kantor',
                    'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
                    )->join(
                        'users as penerima',
                        'header_pengiriman_barang.penerima', '=', 'penerima.id'
                    )->join(
                        'users as pengirim',
                        'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
                    )->join(
                        'users as admin',
                        'header_pengiriman_barang.created_by', '=', 'admin.id'
                    )->where('header_pengiriman_barang.pengirim', '=', $user->id)
                    ->whereBetween('header_pengiriman_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_pengiriman_barang.tgl_surat','desc')
                    ->paginate(15);
            }
            return $peminjaman;
        }
    }
    
    // Get Data Pengiriman
    public function getDataPengiriman($id){
        $header_pengiriman = DB::table('header_pengiriman_barang')->
        select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','pengirim.name as pengirim',
         'admin.name as admin')
            ->join(
            'kantor',
            'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
            )->join(
                'users as penerima',
                'header_pengiriman_barang.penerima', '=', 'penerima.id'
            )->join(
                'users as pengirim',
                'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
            )->join(
                'users as admin',
                'header_pengiriman_barang.created_by', '=', 'admin.id'
            )->where('nomor_surat','=',$id)->get();


        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_pengiriman = DB::table('detail_pengiriman_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_pengiriman_barang.kode_barang', '=', 'items.kode_barang');
        })->where('nomor_surat','=',$id)->get();

        $data_pengiriman = [
            'header_pengiriman'  => $header_pengiriman,
            'detail_pengiriman'   => $detail_pengiriman
        ];
        return $data_pengiriman;
    }

    // Get Header Pengiriman
    public function getHeaderPengiriman($id){

        $header_pengiriman = SendHeader::findorFail($id);
        return $header_pengiriman->getAttributes('nomor_surat');
    }

    // get Detail Pengiriman
    public function getDetailPengiriman($id){
        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_pengiriman = DB::table('detail_pengiriman_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_pengiriman_barang.kode_barang', '=', 'items.kode_barang');
        })->where('detail_pengiriman_barang.nomor_surat','=',$id)->get();
        return $detail_pengiriman;
    }

    // Upload Bukti Kirim
    public function uploadBukti(Request $request, $id)
    {
        $user =  auth('api')->user();

        $this->validate($request,[
            'file_bukti' => 'required'
        ]);

        $item = SendHeader::findorFail($id);
        $currentFile = $item->file_bukti;
        if ($request->file('file_bukti')) {
            $file = $request->file('file_bukti');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file = $request->file('file_bukti')->move(
                public_path('file/bukti-pengiriman/'), $filename
            );


            $itemFile = public_path('file/bukti-pengiriman/').$currentFile; 
            if(file_exists($itemFile)){
                @unlink($itemFile);
            }
            
        }
       
       $item->update([
            'file_bukti' => $filename,
            'status' => 'Tahap Pengiriman'
        ]);
        // return $item;
        return['message'=> "Success"];
    }

    // Delete Items Send
    public function deleteItemsSend($id)
    {
        $this->authorize('isAdmin');

        $lokasi_kirim = DB::table('header_pengiriman_barang')->
        select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
        'admin.name as admin','admin.id as admin_id', 'header_pengiriman_barang.created_at', 'file_bukti')
            ->join(
            'kantor',
            'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
            )->join(
                'users as penerima',
                'header_pengiriman_barang.penerima', '=', 'penerima.id'
            )->join(
                'users as pengirim',
                'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
            )->join(
                'users as admin',
                'header_pengiriman_barang.created_by', '=', 'admin.id'
            )->where('header_pengiriman_barang.nomor_surat','=',$id)->get();

        foreach ($lokasi_kirim as $lokasi_kirim){
            $kantor = $lokasi_kirim->kantor;
        }

        $user =  auth('api')->user();
        $sendRents = SendDetail::findOrFail($id);
        $kodebarang = $sendRents->kode_barang;

        $items = Items::findOrFail($kodebarang);
        $items->dipinjam = 0;
        $items->update();

        $tgl = Carbon::now();
        $tgl_batal = date('Y-m-d', strtotime($tgl));

        $log = new ItemsHistoryLog;
        $log->kode_barang = $kodebarang;
        $log->riwayat = 'Barang dengan kode barang : '.$kodebarang.'. batal dikirim ke '.$kantor.' pada tanggal :'. $tgl_batal;
        $log->tanggal = $tgl_batal;
        $log->user_id = $user->id;
        $log->save();
        
        $sendRents->forceDelete();

        return ['message' => ' Items Deleted'];
       
    }
    // Konfirmasi Penerimaan Barang
    public function konfirmasiPenerimaan(Request $request, $id)
    {
        $user =  auth('api')->user();

        $this->validate($request,[
            'validasi' => 'required'
        ]);

        $item = SendHeader::findorFail($id);
        $tgl = Carbon::now();
        $tgl_terima = date('Y-m-d', strtotime($tgl));

        $lokasi_kirim = DB::table('header_pengiriman_barang')->
        select('nomor_surat','tgl_surat', 'kantor', 'keterangan','penerima.name as penerima','penerima.id as penerima_id','pengirim.name as pengirim','status','permintaan_id','validasi','kantor.id as kantor_id',
        'admin.name as admin','admin.id as admin_id', 'header_pengiriman_barang.created_at', 'file_bukti')
            ->join(
            'kantor',
            'header_pengiriman_barang.kantor_id', '=', 'kantor.id'
            )->join(
                'users as penerima',
                'header_pengiriman_barang.penerima', '=', 'penerima.id'
            )->join(
                'users as pengirim',
                'header_pengiriman_barang.pengirim', '=', 'pengirim.id'
            )->join(
                'users as admin',
                'header_pengiriman_barang.created_by', '=', 'admin.id'
            )->where('header_pengiriman_barang.nomor_surat','=',$id)->get();

        foreach ($lokasi_kirim as $lokasi_kirim){
            $kantor = $lokasi_kirim->kantor;
        }


        $validasi =  $request->get('validasi');
        
        $valueValidasi ='';
        if($validasi == 'Sudah Diterima'){
            $valueValidasi = 1;
        }else{
            $valueValidasi = 2;
        }
        
        $kantor_penerima = $item->kantor_id;
        $item->update([
            'status' => $validasi,
            'validasi' => $valueValidasi,
            'tgl_terima' => $tgl_terima,
        ]);

        $barang = $this->getDetailPengiriman($id);

        foreach($barang as $barang){
            $barang_kirim = Items::findorFail($barang->kode_barang);
            $barang_kirim->dipinjam = 0;
            $barang_kirim->kantor_id = $kantor_penerima;
            $barang_kirim->update();
            
           
            $log = new ItemsHistoryLog;
            $log->kode_barang = $barang->kode_barang;
            $log->riwayat = 'Barang dengan kode barang : '.$barang->kode_barang.'. telah diterima '.$kantor.' pada tanggal :'. $tgl_terima;
            $log->tanggal = $tgl_terima;
            $log->user_id = $user->id;
            $log->save();
        }

        
        
        // return $item;
        return['message'=> "Success"];
    }


    // User Location
    public function getUserLocation(){
        $user =  auth('api')->user();

        $users = DB::table('users')->select('users.id', 'users.kantor_id', 'kantor.kota')
        ->leftJoin(
            'kantor',
            'kantor.id', '=', 'users.kantor_id'
        )->where('users.id','=',$user->id)->get();
        
        foreach ($users as $userr){
            $kantor = $userr->kantor_id;
        }
        
        return $kantor;
    }
    


}
