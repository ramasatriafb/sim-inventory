<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            return Category::latest()->paginate(5);
        }
    }

    public function getDataKategoriInventaris()
    {
            return Category::all();
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kategori' => 'required|string|max:191'
        ]);

        return Category::create([
            'kategori' => $request['kategori'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);

        $this->validate($request,[
            'kategori' => 'required|string|max:191'
        ]);

        $category->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $category = Category::findOrFail($id);

        //delete user
        $category->delete();

        return ['message' => ' Kategori Deleted'];
    }

    public function search(){
        if ($search = \Request::get('q')){
            $categorys = Category::where(function($query) use ($search){
                $query->where('kategori','LIKE', "%$search%");
            })->paginate(5);
        }else{
            $categorys = Category::latest()->paginate(5);
        }
        return $categorys;
    }
}
