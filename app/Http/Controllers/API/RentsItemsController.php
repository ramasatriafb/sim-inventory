<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\RentsHeader;
use App\RentsDetail;
use App\Items;
use App\RecapRentsByUser;
use App\RecapDetailRentsByUser;
use App\RecapItemRentsByProject;
use App\ItemsHistoryLog;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class RentsItemsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $user =  auth('api')->user();
        $firstDay = Carbon::now()->startOfMonth();
        $lastDay = Carbon::now()->endOfMonth();
        $firstDayConvert = date('Y-m-d', strtotime($firstDay));
        $lastDayConvert = date('Y-m-d', strtotime($lastDay));

        if (\Gate::allows('isAdmin') ){
            $peminjaman = DB::table('header_peminjaman_barang')->
            select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
            'admin.name as admin','admin.id as admin_id', 'header_peminjaman_barang.created_at', 'file_bukti')
                ->join(
                'proyek',
                'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                )->join(
                    'users as penanggungjawab',
                    'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as peminjam',
                    'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                )->join(
                    'users as admin',
                    'header_peminjaman_barang.created_by', '=', 'admin.id'
                )->whereBetween('header_peminjaman_barang.tgl_surat',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_peminjaman_barang.created_at','desc')->paginate(15);
            
            return $peminjaman;
        }else if (\ Gate::allows('isUser')){
            $peminjaman = DB::table('header_peminjaman_barang')->
            select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
            'admin.name as admin','admin.id as admin_id', 'header_peminjaman_barang.created_at', 'file_bukti')
                ->join(
                'proyek',
                'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                )->join(
                    'users as penanggungjawab',
                    'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as peminjam',
                    'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                )->join(
                    'users as admin',
                    'header_peminjaman_barang.created_by', '=', 'admin.id'
                )->where('header_peminjaman_barang.peminjam', '=', $user->id)
                ->whereBetween('header_peminjaman_barang.tgl_surat',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_peminjaman_barang.created_at','desc')->paginate(15);
            
            return $peminjaman;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();
        $lokasi = $this->getUserLocation();
        $rules = [
            'nomor_surat' => 'required|string|max:191',
            'tgl_surat' => 'required|string|max:191',
            'proyek_id' => 'required',
            'penanggung_jawab' => 'required',
            'peminjam' => 'required',
            'keterangan'=> 'required',
            'barang'=>'required'
        ];
    
        $customMessages = [
            'barang.required' => 'Barang Harus Dipilih'
        ];
    
        $this->validate($request, $rules, $customMessages);
        
        $proyek = $request->get('proyek_id')['id'];
        $penanggung_jawab = $request->get('penanggung_jawab')['id'];
        $peminjam = $request->get('peminjam')['id'];
        $tgl_pinjam_beforeFormat = $request->get('tgl_surat');
        $tgl_pinjam = date('Y-m-d', strtotime($tgl_pinjam_beforeFormat));
        $keterangan = $request->get('keterangan');
        $barang = $request->get('barang');

        $header_peminjaman = new RentsHeader;
        $header_peminjaman->nomor_surat = $request->get('nomor_surat');
        $header_peminjaman->tgl_surat = $tgl_pinjam;
        $header_peminjaman->proyek_id = $proyek;
        $header_peminjaman->keterangan = $keterangan;
        $header_peminjaman->penanggung_jawab = $penanggung_jawab;
        $header_peminjaman->peminjam = $peminjam;
        $header_peminjaman->aktif = 1;
        $header_peminjaman->created_by = $user->id;
        $header_peminjaman->save();

        \LogActivity::addToLog('Data '.$request->get('nomor_surat').' Berhasil ditambahkan ke tabel Surat Peminjaman Barang.');

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_pinjam){
            $detail_peminjaman = new RentsDetail;    
            $detail_peminjaman->nomor_surat = $request->get('nomor_surat');
            $detail_peminjaman->kode_barang = $item{$no};
            $detail_peminjaman->lokasi_sebelumnya = $lokasi;
            $detail_peminjaman->created_by = $user->id;
            $detail_peminjaman->save();
            $barang_dipinjam = Items::findorFail($item{$no});
            $barang_dipinjam->dipinjam = 1;
            $barang_dipinjam->update();
            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Peminjaman Barang dengan kode barang : '.$item{$no}.'. pada tanggal :'. $tgl_pinjam;
            $log->tanggal = $tgl_pinjam;
            $log->user_id = $peminjam;
            $log->save();
            $no++;
        }
        return ['message'=> "Success"];

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');
        $header_peminjaman = RentsHeader::findorFail($id);
        $user =  auth('api')->user();
        $lokasi = $this->getUserLocation();
        $rules = [
            'tgl_surat' => 'required|string|max:191',
            'proyek_id' => 'required',
            'penanggung_jawab' => 'required',
            'peminjam' => 'required',
            'keterangan'=> 'required',
        ];
    
    
        $this->validate($request, $rules);
        
        if(is_array( $request->get('proyek_id'))){
            $proyek = $request->get('proyek_id')['id'];
        
        }else{
            $proyek = $request->get('proyek_id');
        }

        if(is_array($request->get('penanggung_jawab'))){
            $penanggung_jawab = $request->get('penanggung_jawab')['id'];
        }else{
            $penanggung_jawab = $request->get('penanggung_jawab');
        }

        if(is_array(($request->get('peminjam')))){
            $peminjam = $request->get('peminjam')['id'];
        }else{
            $peminjam = $request->get('peminjam');
        }

        $tgl_pinjam_beforeFormat = $request->get('tgl_surat');
        $tgl_pinjam = date('Y-m-d', strtotime($tgl_pinjam_beforeFormat));
        $keterangan = $request->get('keterangan');
        $barang = $request->get('barang');

        $header_peminjaman->tgl_surat = $tgl_pinjam;
        $header_peminjaman->proyek_id = $proyek;
        $header_peminjaman->keterangan = $keterangan;
        $header_peminjaman->penanggung_jawab = $penanggung_jawab;
        $header_peminjaman->peminjam = $peminjam;
        $header_peminjaman->aktif = 1;
        $header_peminjaman->updated_by = $user->id;
        $header_peminjaman->update();

        \LogActivity::addToLog('Data '.$id.' Berhasil diperbarui ke tabel Surat Peminjaman Barang.');

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_pinjam){
            $detail_peminjaman = new RentsDetail;    
            $detail_peminjaman->nomor_surat = $request->get('nomor_surat');
            $detail_peminjaman->kode_barang = $item{$no};
            $detail_peminjaman->lokasi_sebelumnya = $lokasi;
            $detail_peminjaman->created_by = $user->id;
            $detail_peminjaman->save();
            $barang_dipinjam = Items::findorFail($item{$no});
            $barang_dipinjam->dipinjam = 1;
            $barang_dipinjam->lokasi_barang = $lokasi;
            $barang_dipinjam->update();
            
            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Peminjaman Barang dengan kode barang : '.$item{$no}.'. pada tanggal :'. $tgl_pinjam;
            $log->tanggal = $tgl_pinjam;
            $log->user_id = $peminjam;
            $log->save();

            $no++;
        }
        return ['message'=> "Success"];
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');

        $user =  auth('api')->user();

        $itemsRents = RentsHeader::findOrFail($id);
        $lokasi_sebelumnya = $itemsRents->lokasi_sebelumnya;
        
        $fileName =  public_path('file/bukti-peminjaman/').$itemsRents->file_bukti;
        if(file_exists($fileName)){
            @unlink($fileName);
        }

        $detailRents = RentsDetail::where('nomor_surat','=',$id)->get();
        $detailRentsGet = RentsDetail::where('nomor_surat','=',$id)->first();
       
        foreach ($detailRents as $detail){
            $items[] = $detail->kode_barang;
        }

        foreach ($items as $item){
            $itemsRent = Items::findOrFail($item);
            $itemsRent->dipinjam = 0;
            $items->kantor_id = $lokasi_sebelumnya;
            $itemsRent->update();

            $tgl = Carbon::now();
            $tgl_batal = date('Y-m-d', strtotime($tgl));

            $log = new ItemsHistoryLog;
            $log->kode_barang = $item;
            $log->riwayat = 'Barang dengan kode barang : '.$item.'. batal dipinjam pada tanggal :'. $tgl_batal;
            $log->tanggal = $tgl_batal;
            $log->user_id = $user->id;
            $log->save();
            
            $detailRentsGet = RentsDetail::where('nomor_surat','=',$id)->first();
            $detailRentsGet->forceDelete();
        }

        \LogActivity::addToLog('Data '.$id.' Berhasil dihapus dari tabel Surat Peminjaman Barang.');


        $itemsRents->forceDelete();
        return ['message' => ' Rents Items Deleted'];
       
    }

    public function search(){
        $user =  auth('api')->user();
        if (\Gate::allows('isAdmin') ){
            if ($search = \Request::get('q')){
            $items = DB::table('header_peminjaman_barang')->
            select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
            'admin.name as admin','admin.id as admin_id','header_peminjaman_barang.created_at')
                ->join(
                'proyek',
                'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                )->join(
                    'users as penanggungjawab',
                    'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as peminjam',
                    'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                )->join(
                    'users as admin',
                    'header_peminjaman_barang.created_by', '=', 'admin.id'
                )->where(function($query) use ($search){
                    $query->where('header_peminjaman_barang.nomor_surat','LIKE', "%$search%");
                    })->orderBy('header_peminjaman_barang.created_at','desc')->paginate(15);
            }else{
                $items =  DB::table('header_peminjaman_barang')->
                select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
                'admin.name as admin','admin.id as admin_id','header_peminjaman_barang.created_at')
                ->join(
                'proyek',
                'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                )->join(
                    'users as penanggungjawab',
                    'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as peminjam',
                    'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                )->join(
                    'users as admin',
                    'header_peminjaman_barang.created_by', '=', 'admin.id'
                )->orderBy('header_peminjaman_barang.created_by','desc')->paginate(15);
            }
            return $items;
        }else if (\Gate::allows('isUser') ){
            if ($search = \Request::get('q')){
                $items = DB::table('header_peminjaman_barang')->
                select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
                'admin.name as admin','admin.id as admin_id','header_peminjaman_barang.created_at')
                    ->join(
                    'proyek',
                    'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as peminjam',
                        'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                    )->join(
                        'users as admin',
                        'header_peminjaman_barang.created_by', '=', 'admin.id'
                    )->where('header_peminjaman_barang.peminjam', '=', $user->id)
                    ->where(function($query) use ($search){
                        $query->where('header_peminjaman_barang.nomor_surat','LIKE', "%$search%");
                        })->orderBy('header_peminjaman_barang.created_at','desc')->paginate(15);
                }else{
                    $items =  DB::table('header_peminjaman_barang')->
                    select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
                    'admin.name as admin','admin.id as admin_id','header_peminjaman_barang.created_at')
                    ->join(
                    'proyek',
                    'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as peminjam',
                        'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                    )->join(
                        'users as admin',
                        'header_peminjaman_barang.created_by', '=', 'admin.id'
                    )->where('header_peminjaman_barang.peminjam', '=', $user->id)
                    ->orderBy('header_peminjaman_barang.created_by','desc')->paginate(15);
                }
                return $items;
        }
    }
    
    public function getDataPeminjaman($id){
        $header_peminjaman = DB::table('header_peminjaman_barang')->
        select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
         'admin.name as admin')
            ->join(
            'proyek',
            'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
            )->join(
                'users as penanggungjawab',
                'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
            )->join(
                'users as peminjam',
                'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
            )->join(
                'users as admin',
                'header_peminjaman_barang.created_by', '=', 'admin.id'
            )->where('nomor_surat','=',$id)->get();


        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_peminjaman = DB::table('detail_peminjaman_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_peminjaman_barang.kode_barang', '=', 'items.kode_barang');
        })->where('nomor_surat','=',$id)->get();

        $data_peminjaman = [
            'header_peminjaman'  => $header_peminjaman,
            'detail_peminjaman'   => $detail_peminjaman
        ];
        return $data_peminjaman;
    }

    public function getDetailPeminjaman($id){
        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_peminjaman = DB::table('detail_peminjaman_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_peminjaman_barang.kode_barang', '=', 'items.kode_barang');
        })->where('detail_peminjaman_barang.nomor_surat','=',$id)->get();
        return $detail_peminjaman;
    }

    public function filter($id_proyek, $tgl_awal, $tgl_akhir){
        $user =  auth('api')->user();
        if (\Gate::allows('isAdmin') ){
            if($id_proyek != 'all'){
                $peminjaman = DB::table('header_peminjaman_barang')->
                select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
                'admin.name as admin','admin.id as admin_id','header_peminjaman_barang.created_at', 'file_bukti')
                    ->join(
                    'proyek',
                    'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as peminjam',
                        'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                    )->join(
                        'users as admin',
                        'header_peminjaman_barang.created_by', '=', 'admin.id'
                    )->where('header_peminjaman_barang.proyek_id','=',$id_proyek
                    )->whereBetween('header_peminjaman_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_peminjaman_barang.tgl_surat','desc')
                    ->paginate(15);
            }else{
                $peminjaman = DB::table('header_peminjaman_barang')->
                select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
                'admin.name as admin','admin.id as admin_id','header_peminjaman_barang.created_at', 'file_bukti')
                    ->join(
                    'proyek',
                    'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as peminjam',
                        'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                    )->join(
                        'users as admin',
                        'header_peminjaman_barang.created_by', '=', 'admin.id'
                    )->whereBetween('header_peminjaman_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_peminjaman_barang.tgl_surat','desc')
                    ->paginate(15);
            }
            return $peminjaman;
        } else if (\Gate::allows('isUser') ){
            if($id_proyek != 'all'){
                $peminjaman = DB::table('header_peminjaman_barang')->
                select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
                'admin.name as admin','admin.id as admin_id','header_peminjaman_barang.created_at', 'file_bukti')
                    ->join(
                    'proyek',
                    'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as peminjam',
                        'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                    )->join(
                        'users as admin',
                        'header_peminjaman_barang.created_by', '=', 'admin.id'
                    )->where('header_peminjaman_barang.proyek_id','=',$id_proyek
                    )->where('header_peminjaman_barang.peminjam', '=', $user->id)
                    ->whereBetween('header_peminjaman_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_peminjaman_barang.tgl_surat','desc')
                    ->paginate(15);
            }else{
                $peminjaman = DB::table('header_peminjaman_barang')->
                select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
                'admin.name as admin','admin.id as admin_id','header_peminjaman_barang.created_at', 'file_bukti')
                    ->join(
                    'proyek',
                    'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                    )->join(
                        'users as penanggungjawab',
                        'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as peminjam',
                        'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                    )->join(
                        'users as admin',
                        'header_peminjaman_barang.created_by', '=', 'admin.id'
                    ) ->where('header_peminjaman_barang.peminjam', '=', $user->id)
                    ->whereBetween('header_peminjaman_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                    ->orderBy('header_peminjaman_barang.tgl_surat','desc')
                    ->paginate(15);
            }
            return $peminjaman;
        }
    }

    public function uploadBukti(Request $request, $id)
    {
        $user =  auth('api')->user();

        $this->validate($request,[
            'file_bukti' => 'required'
        ]);

        $item = RentsHeader::findorFail($id);
        $currentFile = $item->file_bukti;
        if ($request->file('file_bukti')) {
            $file = $request->file('file_bukti');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file = $request->file('file_bukti')->move(
                public_path('file/bukti-peminjaman/'), $filename
            );


            $itemFile = public_path('file/bukti-peminjaman/').$currentFile; 
            if(file_exists($itemFile)){
                @unlink($itemFile);
            }
            
        }
       
       $item->update([
            'file_bukti' => $filename,
        ]);
        // return $item;
        return['message'=> "Success"];
    }

    public function getHeaderPeminjaman($id){

        $header_peminjaman = RentsHeader::findorFail($id);
        return $header_peminjaman->getAttributes('nomor_surat');
    }

    public function deleteItemsRents($id)
    {
        $this->authorize('isAdmin');

        $user =  auth('api')->user();
        $itemsRents = RentsDetail::findOrFail($id);
        $kodebarang = $itemsRents->kode_barang;
        $lokasi_sebelumnya = $itemsRents->lokasi_sebelumnya;

        $items = Items::findOrFail($kodebarang);
        $items->dipinjam = 0;
        $items->kantor_id = $lokasi_sebelumnya;
        $items->update();

        $tgl = Carbon::now();
        $tgl_batal = date('Y-m-d', strtotime($tgl));

        $log = new ItemsHistoryLog;
        $log->kode_barang = $kodebarang;
        $log->riwayat = 'Barang dengan kode barang : '.$kodebarang.'. batal dipinjam pada tanggal :'. $tgl_batal;
        $log->tanggal = $tgl_batal;
        $log->user_id = $user->id;
        $log->save();
        
        $itemsRents->forceDelete();

        return ['message' => ' Items Deleted'];
       
    }

    // get Dashboard Jumlah Barang yang dipinjam berdasarkan nama Proyek
    public function getRentsItemsByProject(){
        $peminjaman = DB::table('header_peminjaman_barang')->
        selectRaw('proyek.nama_proyek, count(*) as total')
            ->leftjoin(
            'proyek',
            'header_peminjaman_barang.proyek_id', '=', 'proyek.id')
            ->join (
                'detail_peminjaman_barang',
                'header_peminjaman_barang.nomor_surat','detail_peminjaman_barang.nomor_surat'
            )
            ->join (
                'barang',
                'detail_peminjaman_barang.kode_barang','barang.kode_barang'
            )->where('barang.dipinjam','=',1)
            
            ->groupBy('proyek.nama_proyek', 'proyek.created_at')
            ->orderBy('proyek.created_at','desc')
            ->paginate(7);

        return $peminjaman;
    }

    // Rekap Peminjaman User

    public function recapItemsUser(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            return RecapRentsByUser::paginate(9);
        }
    }
    
    // find Rekap Peminjaman User

    public function findrecapItemsUser(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            if ($search = \Request::get('q')){
                $items = DB::table('rekappeminjamanuserView')->select('id','name','jabatan','no_telp','jumlah','photo')
                ->where(function($query) use ($search){
                    $query->where('name','LIKE', "%$search%");
                })->orderBy('name','asc')->paginate(9);
            }else{
                $items = RecapRentsByUser::paginate(9);
            }
            return $items;
        }
    }

    // Detail Peminjaman History User Saat Ini

    public function getDetailHistoryPeminjamanUser($id){
        $items = DB::table('detailrekappeminjamanuserView')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
        ->where('detailrekappeminjamanuserView.id','=',$id)->get();
        // dd($items) ;
        return $items;
        
    }
    // History Peminjaman User
    public function getHistoryPeminjamanUser($id){
        $items = DB::table('semuarekappeminjamanuserView')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 
         'foto_barang', 'tgl_surat', 'dipinjam')
        ->where('semuarekappeminjamanuserView.id','=',$id)->paginate(10);
        // dd($items) ;
        return $items;
        
    }

      // Rekap Peminjaman Proyek

      public function recapItemsProject(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            return RecapItemRentsByProject::paginate(9);
        }
    }

     // find Rekap Peminjaman Proyek

     public function findrecapItemsProject(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            if ($search = \Request::get('q')){
                $items = DB::table('rekappeminjamanProyekView')->select('id','nama_proyek','lokasi','penanggung_jawab','jumlah')
                ->where(function($query) use ($search){
                    $query->where('nama_proyek','LIKE', "%$search%");
                })->orderBy('nama_proyek','asc')->paginate(9);
            }else{
                $items = RecapItemRentsByProject::paginate(9);
            }
            return $items;
        }
    }
  
     // Detail Peminjaman History Proyek Saat Ini

     public function getDetailHistoryPeminjamanProyek($id){
        $items = DB::table('detailrekappeminjamanProyek')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
        ->where('detailrekappeminjamanProyek.id','=',$id)->get();
        // dd($items) ;
        return $items;
        
    }

     // History Peminjaman Proyek
     public function getHistoryPeminjamanProyek($id){
        $items = DB::table('semuarekappeminjamanProyekView')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 
         'foto_barang', 'tgl_surat', 'dipinjam')
        ->where('semuarekappeminjamanProyekView.id','=',$id)->paginate(10);
        // dd($items) ;
        return $items;
        
    }

    
    // User Location

    public function getUserLocation(){
        $user =  auth('api')->user();

        $users = DB::table('users')->select('users.id', 'users.kantor_id', 'kantor.kota')
        ->leftJoin(
            'kantor',
            'kantor.id', '=', 'users.kantor_id'
        )->where('users.id','=',$user->id)->get();
        
        foreach ($users as $userr){
            $kantor = $userr->kota;
        }
        
        return $kantor;
    }

}
