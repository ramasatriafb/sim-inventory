<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Project;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('isAdmin') || \ Gate::allows('isAuthor')){
            return Project::latest()->paginate(5);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_proyek' => 'required|string|max:191',
            'lokasi' => 'required|string|max:191',
            'penanggung_jawab' => 'required|string|max:191'
        ]);

        return Project::create([
            'nama_proyek' => $request['nama_proyek'],
            'lokasi' => $request['lokasi'],
            'penanggung_jawab' => $request['penanggung_jawab']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);
        return $project;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::findOrFail($id);

        $this->validate($request,[
            'nama_proyek' => 'required|string|max:191',
            'lokasi' => 'required|string|max:191',
            'penanggung_jawab' => 'required|string|max:191'
        ]);

        $project->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $project = Project::findOrFail($id);

        //delete user
        $project->delete();

        return ['message' => ' Proyek dihapus'];
    }

    public function search(){
        if ($search = \Request::get('q')){
            $projects = Project::where(function($query) use ($search){
                $query->where('nama_proyek','LIKE', "%$search%")->orwhere('lokasi','LIKE', "%$search%")
                ->orwhere('penanggung_jawab','LIKE', "%$search%");
            })->paginate(5);
        }else{
            $projects = Project::latest()->paginate(5);
        }
        return $projects;
    }

    public function getProyek(){
        return Project::all();
    }
}