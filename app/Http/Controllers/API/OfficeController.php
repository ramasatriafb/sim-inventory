<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Office;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('isAdmin') || \ Gate::allows('isAuthor')){
            return Office::latest()->paginate(5);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kantor' => 'required|string|max:191',
            'alamat' => 'required|string|max:191',
            'kota' => 'required|string|max:191'
        ]);

        \LogActivity::addToLog($request->get('kantor').' Berhasil ditambahkan ke tabel kantor cabang.');

        return Office::create([
            'kantor' => $request['kantor'],
            'alamat' => $request['alamat'],
            'kota' => $request['kota']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $office = Office::findOrFail($id);
        return $office;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $office = Office::findOrFail($id);

        $this->validate($request,[
            'kantor' => 'required|string|max:191',
            'alamat' => 'required|string|max:191',
            'kota' => 'required|string|max:191'
        ]);


        \LogActivity::addToLog('Data '.$request->get('kantor').' Berhasil diperbarui ke tabel kantor cabang.');

        $office->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $office = Office::findOrFail($id);

        \LogActivity::addToLog('Data '.$office->kantor.' Berhasil dihapus dari tabel kantor cabang.');

        //delete user
        $office->delete();

        return ['message' => ' Kota dihapus'];
    }

    public function getDataKantor()
    {
            return Office::all();
        
    }

      // Get Office
    public function getKantor(){
        $offices =  Office::select('id','kantor')->get();
        $officeData = [];
        foreach($offices as $office){
            $officeData[] = $office;
        }

        return $officeData;
    }

}
