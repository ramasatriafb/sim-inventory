<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ReferenceNumber;
use Carbon\Carbon;
use App\RentsHeader;
use App\ReturnItemsHeader;
use App\ServiceItemsHeader;
use App\HeaderRequestItems;
use App\SendHeader;
use Illuminate\Support\Facades\DB;

class ReferenceNumberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('isAdmin') || \ Gate::allows('isUser')){
            return ReferenceNumber::latest()->paginate(5);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kategori_surat' => 'required|string|max:191',
            'kode_surat' => 'required|string|max:5',
            'unit_surat' => 'required|string|max:5'
        ]);

        return ReferenceNumber::create([
            'kategori_surat' => $request['kategori_surat'],
            'kode_surat' => $request['kode_surat'],
            'unit_surat' => $request['unit_surat'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $referenceNumber = ReferenceNumber::findOrFail($id);

        $this->validate($request,[
            'kategori_surat' => 'required|string|max:191',
            'kode_surat' => 'required|string|max:5',
            'unit_surat' => 'required|string|max:5'
        ]);

        $referenceNumber->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $referenceNumber = ReferenceNumber::findOrFail($id);

        //delete user
        $referenceNumber->delete();

        return ['message' => ' Referensi Nomor Surat Deleted'];
    }

    public function getReferenceNumber($kategori_surat){

        $this_year = Carbon::now();
        $monthConvert = date('m', strtotime($this_year));
        $yearConvert = date('Y', strtotime($this_year));


        if ($kategori_surat == 'peminjaman'){
            $referenceNumber = ReferenceNumber::where('kategori_surat','LIKE', "%$kategori_surat%")->get();
            foreach ($referenceNumber as $ref ){
                $kode_surat = $ref->kode_surat;
                $unit_surat = $ref->unit_surat;
    
            }
            $count = RentsHeader::where( DB::raw('YEAR(created_at)'), '=', $this_year )->count();
            $ref_numberInc = $count+1;
            
        }else if ($kategori_surat == 'pengembalian'){
            $referenceNumber = ReferenceNumber::where('kategori_surat','LIKE', "%$kategori_surat%")->get();
            foreach ($referenceNumber as $ref ){
                $kode_surat = $ref->kode_surat;
                $unit_surat = $ref->unit_surat;
    
            }
            $count = ReturnItemsHeader::where( DB::raw('YEAR(created_at)'), '=', $this_year )->count();
            $ref_numberInc = $count+1;
            
        }else if ($kategori_surat == 'perbaikan'){
            $referenceNumber = ReferenceNumber::where('kategori_surat','LIKE', "%$kategori_surat%")->get();
            foreach ($referenceNumber as $ref ){
                $kode_surat = $ref->kode_surat;
                $unit_surat = $ref->unit_surat;
    
            }
            $count = ServiceItemsHeader::where( DB::raw('YEAR(created_at)'), '=', $this_year )->count();
            $ref_numberInc = $count+1;
        }else if ($kategori_surat == 'permintaan'){
            $referenceNumber = ReferenceNumber::where('kategori_surat','LIKE', "%$kategori_surat%")->get();
            foreach ($referenceNumber as $ref ){
                $kode_surat = $ref->kode_surat;
                $unit_surat = $ref->unit_surat;
    
            }
            $count = HeaderRequestItems::where( DB::raw('YEAR(created_at)'), '=', $this_year )->count();
            $ref_numberInc = $count+1;
        }else{
            $referenceNumber = ReferenceNumber::where('kategori_surat','LIKE', "%$kategori_surat%")->get();
            foreach ($referenceNumber as $ref ){
                $kode_surat = $ref->kode_surat;
                $unit_surat = $ref->unit_surat;
    
            }
            $count = SendHeader::where( DB::raw('YEAR(created_at)'), '=', $this_year )->count();
            $ref_numberInc = $count+1;
        }
       
       
       
        $ref_numberInc = str_pad($ref_numberInc, 3, '0', STR_PAD_LEFT);
       
        $ref_number = $ref_numberInc.'.'.$kode_surat. '-'. 
        $unit_surat.'-'.$monthConvert.'-'.$yearConvert;
        return $ref_number;
    }
}
