<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ServiceItemsHeader;
use App\ServiceItemsDetail;
use App\Items;
use App\ItemsHistoryLog;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ServiceItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $user =  auth('api')->user();
        $firstDay = Carbon::now()->startOfMonth();
        $lastDay = Carbon::now()->endOfMonth();
        $firstDayConvert = date('Y-m-d', strtotime($firstDay));
        $lastDayConvert = date('Y-m-d', strtotime($lastDay));

        if (\Gate::allows('isAdmin')){
            $perbaikan = DB::table('header_perbaikan_barang')->
            select('nomor_surat','tgl_surat', 'keterangan','nama_tempat_perbaikan','alamat_perbaikan','penanggungjawab.name as penanggungjawab','pembawa.name as pembawa',
            'admin.name as admin','admin.id as admin_id', 'header_perbaikan_barang.created_at', 'file_bukti')
                ->join(
                    'users as penanggungjawab',
                    'header_perbaikan_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pembawa',
                    'header_perbaikan_barang.pihak_pembawa', '=', 'pembawa.id'
                )->join(
                    'users as admin',
                    'header_perbaikan_barang.created_by', '=', 'admin.id'
                )->whereBetween('header_perbaikan_barang.tgl_surat',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_perbaikan_barang.created_at','desc')->paginate(15);
            
            return $perbaikan;
        }else if (\ Gate::allows('isUser')){
            $perbaikan = DB::table('header_perbaikan_barang')->
            select('nomor_surat','tgl_surat', 'keterangan','nama_tempat_perbaikan','alamat_perbaikan','penanggungjawab.name as penanggungjawab','pembawa.name as pembawa',
            'admin.name as admin','admin.id as admin_id', 'header_perbaikan_barang.created_at', 'file_bukti')
                ->join(
                    'users as penanggungjawab',
                    'header_perbaikan_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pembawa',
                    'header_perbaikan_barang.pihak_pembawa', '=', 'pembawa.id'
                )->join(
                    'users as admin',
                    'header_perbaikan_barang.created_by', '=', 'admin.id'
                )->where('header_perbaikan_barang.pihak_pembawa', '=', $user->id)
                ->whereBetween('header_perbaikan_barang.tgl_surat',[$firstDayConvert, $lastDayConvert])
                ->orderBy('header_perbaikan_barang.created_at','desc')->paginate(15);
            
            return $perbaikan;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();
        $rules = [
            'nomor_surat' => 'required|string|max:191',
            'tgl_surat' => 'required|string|max:191',
            'penanggung_jawab' => 'required',
            'pihak_pembawa' => 'required',
            'keterangan'=> 'required',
            'nama_tempat_perbaikan'=> 'required',
            'barang'=>'required'
        ];
    
        $customMessages = [
            'barang.required' => 'Barang Harus Dipilih'
        ];
    
        $this->validate($request, $rules, $customMessages);
        
        $penanggung_jawab = $request->get('penanggung_jawab')['id'];
        $pembawa = $request->get('pihak_pembawa')['id'];
        $tgl_perbaikan_beforeFormat = $request->get('tgl_surat');
        $tgl_perbaikan = date('Y-m-d', strtotime($tgl_perbaikan_beforeFormat));
        $keterangan = $request->get('keterangan');
        $nama_tempat_perbaikan = $request->get('nama_tempat_perbaikan');
        $alamat_perbaikan = $request->get('alamat_perbaikan');
        $barang = $request->get('barang');

        $header_perbaikan = new ServiceItemsHeader;
        $header_perbaikan->nomor_surat = $request->get('nomor_surat');
        $header_perbaikan->tgl_surat = $tgl_perbaikan;
        $header_perbaikan->keterangan = $keterangan;
        $header_perbaikan->nama_tempat_perbaikan = $nama_tempat_perbaikan;
        $header_perbaikan->alamat_perbaikan = $alamat_perbaikan;
        $header_perbaikan->penanggung_jawab = $penanggung_jawab;
        $header_perbaikan->pihak_pembawa = $pembawa;
        $header_perbaikan->aktif = 1;
        $header_perbaikan->created_by = $user->id;
        $header_perbaikan->save();

        \LogActivity::addToLog('Data '.$request->get('nomor_surat').' Berhasil ditambahkan ke tabel Surat Perbaikan Barang.');

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_perbaikan){
            $detail_perbaikan = new ServiceItemsDetail;    
            $detail_perbaikan->nomor_surat = $request->get('nomor_surat');
            $detail_perbaikan->kode_barang = $item{$no};
            $detail_perbaikan->created_by = $user->id;
            $detail_perbaikan->save();
            $barang_perbaikan = Items::findorFail($item{$no});
            $barang_perbaikan->status = 'Perbaikan';
            $barang_perbaikan->dipinjam = 2;
            $barang_perbaikan->update();

            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Perbaikan Barang dengan kode barang : '.$item{$no}.'. pada tanggal :'. $tgl_perbaikan;
            $log->tanggal = $tgl_perbaikan;
            $log->user_id = $pembawa;
            $log->save();

            $no++;
        }
        return ['message'=> "Success"];

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');
        $header_perbaikan = ServiceItemsHeader::findorFail($id);
        $user =  auth('api')->user();
        $rules = [
            'tgl_surat' => 'required|string|max:191',
            'penanggung_jawab' => 'required',
            'pihak_pembawa' => 'required',
            'keterangan'=> 'required',
            'nama_tempat_perbaikan'=> 'required',
        ];
    
    
        $this->validate($request, $rules);
        
        if(is_array($request->get('penanggung_jawab'))){
            $penanggung_jawab = $request->get('penanggung_jawab')['id'];
        }else{
            $penanggung_jawab = $request->get('penanggung_jawab');
        }

        if(is_array(($request->get('pihak_pembawa')))){
            $pihak_pembawa = $request->get('pihak_pembawa')['id'];
        }else{
            $pihak_pembawa = $request->get('pihak_pembawa');
        }

        $tgl_perbaikan_beforeFormat = $request->get('tgl_surat');
        $tgl_perbaikan = date('Y-m-d', strtotime($tgl_perbaikan_beforeFormat));
        $keterangan = $request->get('keterangan');
        $nama_tempat_perbaikan = $request->get('nama_tempat_perbaikan');
        $alamat_perbaikan = $request->get('alamat_perbaikan');
        $barang = $request->get('barang');

        $header_perbaikan->tgl_surat = $tgl_perbaikan;
        $header_perbaikan->keterangan = $keterangan;
        $header_perbaikan->nama_tempat_perbaikan = $nama_tempat_perbaikan;
        $header_perbaikan->alamat_perbaikan = $alamat_perbaikan;
        $header_perbaikan->penanggung_jawab = $penanggung_jawab;
        $header_perbaikan->pihak_pembawa = $pihak_pembawa;
        $header_perbaikan->aktif = 1;
        $header_perbaikan->updated_by = $user->id;
        $header_perbaikan->update();

        \LogActivity::addToLog('Data '.$id.' Berhasil diperbarui ke tabel Surat Perbaikan Barang.');

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_perbaikan){
            $detail_peminjaman = new ServiceItemsDetail;    
            $detail_peminjaman->nomor_surat = $request->get('nomor_surat');
            $detail_peminjaman->kode_barang = $item{$no};
            $detail_peminjaman->created_by = $user->id;
            $detail_peminjaman->save();
            $barang_perbaikan = Items::findorFail($item{$no});
            $barang_perbaikan->dipinjam = 2;
            $barang_perbaikan->status = 'Perbaikan';

            $log = new ItemsHistoryLog;
            $log->kode_barang = $item{$no};
            $log->riwayat = 'Perbaikan Barang dengan kode barang : '.$item{$no}.'. pada tanggal :'. $tgl_perbaikan;
            $log->tanggal = $tgl_perbaikan;
            $log->user_id = $pihak_pembawa;
            $log->save();

            $barang_perbaikan->update();
            $no++;
        }
        return ['message'=> "Success"];
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();
        $itemsServices = ServiceItemsHeader::findOrFail($id);
        
        $fileName =  public_path('file/bukti-perbaikan/').$itemsServices->file_bukti;
        if(file_exists($fileName)){
            @unlink($fileName);
        }

        $detailServices = ServiceItemsDetail::where('nomor_surat','=',$id)->get();
        $detailServicesGet = ServiceItemsDetail::where('nomor_surat','=',$id)->first();
       
        foreach ($detailServices as $detail){
            $items[] = $detail->kode_barang;
        }

        foreach ($items as $item){
            $itemsService = Items::findOrFail($item);
            $itemsService->dipinjam = 0;
            $itemsService->status = 'Baik';
            $itemsService->update();

            $tgl = Carbon::now();
            $tgl_batal = date('Y-m-d', strtotime($tgl));
            $log = new ItemsHistoryLog;
            $log->kode_barang = $item;
            $log->riwayat = 'Barang dengan kode barang : '.$item.'. batal diperbaiki pada tanggal :'. $tgl_batal;
            $log->tanggal = $tgl_batal;
            $log->user_id = $user->id;
            $log->save();

            $detailServicesGet = ServiceItemsDetail::where('nomor_surat','=',$id)->first();
            $detailServicesGet->forceDelete();
        }

        $itemsServices->forceDelete();
        \LogActivity::addToLog('Data '.$id.' Berhasil dihapus dari tabel Surat Perbaikan Barang.');

        return ['message' => ' Service Items Deleted'];
       
    }

    public function getDataPerbaikan($id){
        $header_perbaikan = DB::table('header_perbaikan_barang')->
        select('nomor_surat','tgl_surat', 'keterangan','nama_tempat_perbaikan','alamat_perbaikan','penanggungjawab.name as penanggungjawab','pembawa.name as pembawa',
         'admin.name as admin')
           ->join(
                'users as penanggungjawab',
                'header_perbaikan_barang.penanggung_jawab', '=', 'penanggungjawab.id'
            )->join(
                'users as pembawa',
                'header_perbaikan_barang.pihak_pembawa', '=', 'pembawa.id'
            )->join(
                'users as admin',
                'header_perbaikan_barang.created_by', '=', 'admin.id'
            )->where('nomor_surat','=',$id)->get();


        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_perbaikan = DB::table('detail_perbaikan_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_perbaikan_barang.kode_barang', '=', 'items.kode_barang');
        })->where('nomor_surat','=',$id)->get();

        $data_perbaikan = [
            'header_perbaikan'  => $header_perbaikan,
            'detail_perbaikan'   => $detail_perbaikan
        ];
        return $data_perbaikan;
    }

    public function getDetailPerbaikan($id){
        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_perbaikan = DB::table('detail_perbaikan_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_perbaikan_barang.kode_barang', '=', 'items.kode_barang');
        })->where('detail_perbaikan_barang.nomor_surat','=',$id)->get();
        return $detail_perbaikan;
    }

    public function filter($tgl_awal, $tgl_akhir){
        $user =  auth('api')->user();
        if (\Gate::allows('isAdmin')){
        
            $perbaikan = DB::table('header_perbaikan_barang')->
            select('nomor_surat','tgl_surat', 'keterangan','penanggungjawab.name as penanggungjawab','pembawa.name as pembawa',
            'admin.name as admin','admin.id as admin_id','header_perbaikan_barang.created_at', 'file_bukti')
                ->join(
                    'users as penanggungjawab',
                    'header_perbaikan_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pembawa',
                    'header_perbaikan_barang.pihak_pembawa', '=', 'pembawa.id'
                )->join(
                    'users as admin',
                    'header_perbaikan_barang.created_by', '=', 'admin.id'
                )->whereBetween('header_perbaikan_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                ->orderBy('header_perbaikan_barang.tgl_surat','desc')
                ->paginate(15);
        
            return $perbaikan;

        }else if (\ Gate::allows('isUser')){
            $perbaikan = DB::table('header_perbaikan_barang')->
            select('nomor_surat','tgl_surat', 'keterangan','penanggungjawab.name as penanggungjawab','pembawa.name as pembawa',
            'admin.name as admin','admin.id as admin_id','header_perbaikan_barang.created_at', 'file_bukti')
                ->join(
                    'users as penanggungjawab',
                    'header_perbaikan_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pembawa',
                    'header_perbaikan_barang.pihak_pembawa', '=', 'pembawa.id'
                )->join(
                    'users as admin',
                    'header_perbaikan_barang.created_by', '=', 'admin.id'
                )->where('header_perbaikan_barang.pihak_pembawa', '=', $user->id)
                ->whereBetween('header_perbaikan_barang.tgl_surat',[$tgl_awal, $tgl_akhir])
                ->orderBy('header_perbaikan_barang.tgl_surat','desc')
                ->paginate(15);
        
            return $perbaikan;
        }
    }

    public function getHeaderPerbaikan($id){
            $header_perbaikan = ServiceItemsHeader::findorFail($id);
            return $header_perbaikan->getAttributes('nomor_surat');
    }

    public function uploadBukti(Request $request, $id)
    {
        $user =  auth('api')->user();

        $this->validate($request,[
            'file_bukti' => 'required'
        ]);

        $item = ServiceItemsHeader::findorFail($id);
        $currentFile = $item->file_bukti;
        if ($request->file('file_bukti')) {
            $file = $request->file('file_bukti');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file = $request->file('file_bukti')->move(
                public_path('file/bukti-perbaikan/'), $filename
            );


            $itemFile = public_path('file/bukti-perbaikan/').$currentFile; 
            if(file_exists($itemFile)){
                @unlink($itemFile);
            }
            
        }
       
       $item->update([
            'file_bukti' => $filename,
        ]);
        // return $item;
        return['message'=> "Success"];
    }

    public function deleteItemsService($id)
    {
        $this->authorize('isAdmin');
        $user =  auth('api')->user();
        $itemsServices = ServiceItemsDetail::findOrFail($id);
        $kodebarang = $itemsServices->kode_barang;
        $items = Items::findOrFail($kodebarang);
        $items->dipinjam = 0;
        $items->status = 'Rusak';
        $items->update();

        $tgl = Carbon::now();
        $tgl_batal = date('Y-m-d', strtotime($tgl));
        $log = new ItemsHistoryLog;
        $log->kode_barang = $kodebarang;
        $log->riwayat = 'Barang dengan kode barang : '.$kodebarang.'. batal diperbaiki pada tanggal :'. $tgl_batal;
        $log->tanggal = $tgl_batal;
        $log->user_id = $user->id;
        $log->save();

        $itemsServices->forceDelete();

        return ['message' => ' Items Deleted'];
       
    }

    public function search(){
        $user =  auth('api')->user();
        if(\ Gate::allows('isAdmin')){
            if ($search = \Request::get('q')){
            $items = DB::table('header_perbaikan_barang')->
            select('nomor_surat','tgl_surat', 'keterangan','nama_tempat_perbaikan','alamat_perbaikan','file_bukti','penanggungjawab.name as penanggungjawab','pembawa.name as pembawa',
            'admin.name as admin','admin.id as admin_id','header_perbaikan_barang.created_at')
                    ->join(
                    'users as penanggungjawab',
                    'header_perbaikan_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pembawa',
                    'header_perbaikan_barang.pihak_pembawa', '=', 'pembawa.id'
                )->join(
                    'users as admin',
                    'header_perbaikan_barang.created_by', '=', 'admin.id'
                )->where(function($query) use ($search){
                    $query->where('header_perbaikan_barang.nomor_surat','LIKE', "%$search%");
                    })->orderBy('header_perbaikan_barang.created_at','desc')->paginate(15);
            }else{
                $items =  DB::table('header_perbaikan_barang')->
                select('nomor_surat','tgl_surat','keterangan','nama_tempat_perbaikan','alamat_perbaikan','file_bukti','penanggungjawab.name as penanggungjawab','pembawa.name as pembawa',
                'admin.name as admin','admin.id as admin_id','header_perbaikan_barang.created_at')
                ->join(
                    'users as penanggungjawab',
                    'header_perbaikan_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as pembawa',
                    'header_perbaikan_barang.pihak_pembawa', '=', 'pembawa.id'
                )->join(
                    'users as admin',
                    'header_perbaikan_barang.created_by', '=', 'admin.id'
                )->orderBy('header_perbaikan_barang.created_by','desc')->paginate(15);
            }
            return $items;
        } else if (\ Gate::allows('isUser')){
            if ($search = \Request::get('q')){
                $items = DB::table('header_perbaikan_barang')->
                select('nomor_surat','tgl_surat', 'keterangan','penanggungjawab.name as penanggungjawab','pembawa.name as pembawa',
                'admin.name as admin','admin.id as admin_id','header_perbaikan_barang.created_at')
                        ->join(
                        'users as penanggungjawab',
                        'header_perbaikan_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as pembawa',
                        'header_perbaikan_barang.pihak_pembawa', '=', 'pembawa.id'
                    )->join(
                        'users as admin',
                        'header_perbaikan_barang.created_by', '=', 'admin.id'
                    )->where('header_perbaikan_barang.pihak_pembawa', '=', $user->id)
                    ->where(function($query) use ($search){
                        $query->where('header_perbaikan_barang.nomor_surat','LIKE', "%$search%");
                        })->orderBy('header_perbaikan_barang.created_at','desc')->paginate(15);
                }else{
                    $items =  DB::table('header_perbaikan_barang')->
                    select('nomor_surat','tgl_surat','keterangan','penanggungjawab.name as penanggungjawab','pembawa.name as pembawa',
                    'admin.name as admin','admin.id as admin_id','header_perbaikan_barang.created_at')
                    ->join(
                        'users as penanggungjawab',
                        'header_perbaikan_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                    )->join(
                        'users as pembawa',
                        'header_perbaikan_barang.pihak_pembawa', '=', 'pembawa.id'
                    )->join(
                        'users as admin',
                        'header_perbaikan_barang.created_by', '=', 'admin.id'
                    )->where('header_perbaikan_barang.pihak_pembawa', '=', $user->id)
                    ->orderBy('header_perbaikan_barang.created_by','desc')->paginate(15);
                }
                return $items;
        }
    }
    
}
