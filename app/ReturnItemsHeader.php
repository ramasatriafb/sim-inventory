<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnItemsHeader extends Model
{
    protected $table = 'header_pengembalian_barang';
    protected $fillable = ['nomor_surat','tgl_pemngembalian','proyek_id','keterangan','penanggung_jawab','pengembali','aktif','file_bukti',
    'updated_by','deleted_by'];
    protected $primaryKey = 'nomor_surat';

    protected $cast = ["tgl_pemngembalian"=> "DATE"];

    public function proyek(){
        return $this->belongsTo('App\Project');
    }

    public function details()
    {
        return $this->hasMany('App\ReturnItemsDetail');
    }
}
