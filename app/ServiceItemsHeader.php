<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceItemsHeader extends Model
{
    protected $table = 'header_perbaikan_barang';
    protected $fillable = ['nomor_surat','tgl_surat','keterangan','penanggung_jawab','pihak_pembawa','aktif','file_bukti',
    'updated_by','deleted_by'];
    protected $primaryKey = 'nomor_surat';

    protected $cast = ["tgl_surat"=> "DATE"];

}
