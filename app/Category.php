<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'kategori_inventaris';

    protected $fillable = [
        'kategori'
    ];

    public function items(){
        return $this->hasMany('App\Items');
    }
}
