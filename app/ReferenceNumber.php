<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferenceNumber extends Model
{
    protected $table = 'ref_nomor_surat';

    protected $fillable = [
        'kategori_surat','kode_surat','unit_surat'
    ];
}
