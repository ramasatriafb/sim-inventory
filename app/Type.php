<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'jenis_barang';

    protected $fillable = [
        'jenis'
    ];

    public function items(){
        return $this->hasMany('App\Items');
    }
}
