<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeaderServiceReturn extends Model
{
    protected $table = 'header_barang_service_kembali';
    protected $fillable = ['tgl_pengembalian','keterangan','penanggung_jawab','pengembali_barang','created_by',
    'updated_by','deleted_by'];

    protected $cast = ["tgl_pengembalian"=> "DATE"];

   
}
